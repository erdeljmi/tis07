// File Broadcast application:
// 1. Reads a file
// 2. Parses a file
// 3. Broadcasts things from the file
// 4. Receives and displays received data
// 5. User can sed the frequency of sending

#include <pthread.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <fcntl.h>
//#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
//#include <malloc.h>

typedef unsigned char uchar;

#define PORT_IN 15000
#define PORT_OUT 15000
//#define PORT_GPS 15055
#define ADDR_OUT "192.168.6.255"

#define THREAD_COUNT 1
#define THREAD_IN_SLEEP 1000
//#define THREAD_OUT_SLEEP 100000
#define MAIN_SLEEP 10000 // was 10000
//#define GPS_SLEEP 1000000

#define BUFF_IN_LEN 30
#define BUFF_OUT_LEN 30
//#define BUFF_GPS_LEN 8

#define MAX_ARRAY_SIZE 250

char buff_out[BUFF_OUT_LEN];

pthread_t threads[THREAD_COUNT];
int i, ret, hUSB=0, fd;

FILE *file_out;
char file_name[60],file_line[120];
char local_time[25];
time_t now;
struct timeval tv;
struct timezone tz;
struct tm *tm;

void * th_in(void *num);
//void * th_out(void *num);
//void * th_gps(void *num);

void file_write(char *line);
char checksum(uchar *buff, char len);
char add_checksum(uchar *buff, char len);

//=============================
int main(int argc, char **argv)
{
    FILE *file;
    char line[250];
    int sending = 0, receiving = 0;
    int c;

    // parse the arguments
    // -S -R <filename>
    char *inFile = NULL;

    printf("========================================\n");
    printf("Usage: ./file_broadcast -R -S <filename>\n");
    printf("========================================\n");
    
    opterr = 0;
    while ((c = getopt (argc, argv, "RS:")) != -1)
    {
        switch (c)
        {
        case 'R':
            receiving = 1;
            break;
        case 'S':
            sending = 1;
            inFile = optarg;
            break;
        case '?':
            if (optopt == 'S')
                fprintf (stderr, "Option -%c requires an argument.\n", optopt);
            else if (isprint (optopt))
                fprintf (stderr, "Unknown option `-%c'.\n", optopt);
            else
                fprintf (stderr,
                         "Unknown option character `\\x%x'.\n",
                         optopt);
            return 1;
        default:
            abort();
        }
    }

    if(sending) printf("SEND - OK\n"); else printf("SEND - DISABLED\n");
    if(receiving) printf("RECV - OK\n"); else printf("RECV - DISABLED\n");

    //for (index = optind; index < argc; index++)
    //    printf ("Non-option argument %s\n", argv[index]);

    // launch receiving thread if receiving requested
    if(receiving)
    {
        // the receive thread is launched
        if((ret = pthread_create(&threads[0], NULL, th_in, (void *) 0))!=0)
        {
            // recv thread could not be launched
          printf("[ERRR] %s",strerror(ret));
          //file_write(file_line);
          //fclose(file_out);
          exit(1);
        }
    }

    // the app will send the contents of the file
    if(sending)
    {
        file = fopen(inFile,"rt");
        //num_runs = atoi(argv[2]);
        //double x, y, z;
        int socket_out;
        int broadcast = 1;
        struct sockaddr_in myaddr_out;

        // socket for broadcast
        if((socket_out=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP))<0)
        {
            printf(file_line,"[ERRR] udp out socket error\n");
            //file_write(file_line);
            //fclose(file_out);
            exit(1);
        }
        if((setsockopt(socket_out,SOL_SOCKET,SO_BROADCAST,&broadcast,sizeof broadcast)) == -1)
        {
            printf("udp out setsockopt error\n");
            exit(1);
        }

        printf("SEND launched.\n");
        memset((char *) &myaddr_out, 0, sizeof(myaddr_out));
        myaddr_out.sin_family=AF_INET;
        myaddr_out.sin_port=htons(PORT_OUT);
        myaddr_out.sin_addr.s_addr=inet_addr(ADDR_OUT);

        while(fgets(line, 250, file) != NULL)
        {
//            // if the file is formatted, getting the double values from the line
//            sscanf(line,"%lf,%lf,%lf\n", &x, &y, &z);
//            // using the values to construct the message
//            sprintf(buff_out,"%.6lf,%.6lf,%.6lf", x, y, z);
//            sendto(socket_out, buff_out, sizeof(buff_out), 0, (struct sockaddr *) &myaddr_out, sizeof(myaddr_out));
            // sending line by line from file
            sendto(socket_out, line, sizeof(line), 0, (struct sockaddr *) &myaddr_out, sizeof(myaddr_out));
            printf("[SEND] %s\n", line);
            //usleep(MAIN_SLEEP);
        }
        fclose(file);
    }
    if(!receiving) return 0;
    
    while(1)
    {
        usleep(MAIN_SLEEP);
    }
    return 0;
}

//======================
void * th_in(void * num)
{
    int socket_in;
    struct sockaddr_in myaddr_in, client_in;
    uchar buff_in[BUFF_IN_LEN];
    socklen_t clilen=sizeof(client_in);
    if((socket_in=socket(AF_INET,SOCK_DGRAM,0))<0)
    {
        fprintf(file_out,"[ERRR] udp in socket error\n");
        fclose(file_out);
        exit(1);
    }
    printf("RECV thread launched.\n");
    memset((char *)&myaddr_in, 0, sizeof(myaddr_in));
    myaddr_in.sin_family=AF_INET;
    myaddr_in.sin_addr.s_addr=htonl(INADDR_ANY);
    myaddr_in.sin_port=htons(PORT_IN);
    if(bind(socket_in,(struct sockaddr *)&myaddr_in,sizeof(myaddr_in))<0)
    {
        fprintf(file_out,"[ERRR] udp in bind error\n");
        fclose(file_out);
        exit(1);
    }
    printf("RECV bind OK\n");
    while(1)
    {
        recvfrom(socket_in,buff_in,BUFF_IN_LEN,0,(struct sockaddr *)&client_in,&clilen);
        printf("[RECV] %s\n", buff_in);
        usleep(THREAD_IN_SLEEP);
    }
}
