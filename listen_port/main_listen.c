// Port listening application
// January 2011
// Milan Erdelj

#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <malloc.h>
#include <arpa/inet.h>

#define HOST_ADDR "127.0.0.1"
#define PORT 5000

//==============================
int main(int argc, char *argv[])
{
  char buff_listen[255], listen_host[20];
  int listen_port, socket_listen;
  if(argc==3)
  {
    listen_port=(int)atof(argv[2]);
    strcpy(listen_host, argv[1]);
  }
  else
  {
    printf("syntax: listen <address> <port>\n");
    exit(1);
  }
  struct sockaddr_in myaddr_listen, client_listen;
  socklen_t clilen_listen=sizeof(client_listen);
  if((socket_listen=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP))<0)
  {
    printf("listen socket error\n");
    exit(1);
  }
  memset((char *)&myaddr_listen, 0, sizeof(myaddr_listen));
  myaddr_listen.sin_family=AF_INET;
  myaddr_listen.sin_addr.s_addr=inet_addr(listen_host);
  myaddr_listen.sin_port=htons(listen_port);
  if(bind(socket_listen,(struct sockaddr *)&myaddr_listen,sizeof(myaddr_listen))<0)
  {
    printf("listen socket bind error\n");
    exit(1);
  }
  printf("listening on port %d...\n",listen_port);
  while(1)
  {    
    recvfrom(socket_listen,buff_listen,255,0,(struct sockaddr *)&client_listen,&clilen_listen);
    printf("%s",buff_listen);
  }
  return 0;
}
