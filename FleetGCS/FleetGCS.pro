#-------------------------------------------------
#
# Project created by QtCreator 2016-07-21T23:20:55
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

QMAKE_CXXFLAGS = -std=c++11
QMAKE_LFLAGS = -std=c++11

TARGET = FleetGCS
TEMPLATE = app

SOURCES += main.cpp\
    FleetControl.cpp \
    Socket.cpp \
    MavlinkServer.cpp \
    qcustomplot.cpp

HEADERS  += \
    include/common/mavlink.h \
    FleetControl.h \
    NeighUAV.h \
    Socket.h \
    geodetic_conv.hpp \
    locked_queue.h \
    MavlinkServer.h \
    qcustomplot.h

FORMS    += \
    fleetcontrol.ui
