//  created:    04/2016
//  updated:    09/2016
//  filename:   MissionPlanner.h
//
//  author:     Milan Erdelj
//
//  version:    $Id: $
//
//  purpose:    Mission planner header
//
//
/*********************************************************************/

#ifndef MISSIONPLANNER_H
#define MISSIONPLANNER_H

#include <QWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QTableWidget>
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QUrlQuery>
#include <vector>
#include <unistd.h>
#include <QTimer>
#include <csignal>
#include "MavlinkServer.h"
#include "NeighUAV.h"
#include "geodetic_conv.hpp"
#include <math.h>

#define INTER_COMMAND_PAUSE 10000 // us

#define PERIOD_SECOND 1000000 // us

#define UAV_INFO_TIMEOUT 2500000

#define HEARTBEAT_DEFAULT_PERIOD 1000000
#define POSITION_DEFAULT_PERIOD 1000000
#define ATTITUDE_DEFAULT_PERIOD 1000000
#define STATUS_DEFAULT_PERIOD 1000000

#define COLUMN_COUNT 8
#define ROW_COUNT 10

#define VOLTAGE_CRITICAL 11.5
#define VOLTAGE_REPLACEMENT 12.3

#define SIGNAL_PERIOD 20 // ms

//#define NET_CONNECT

namespace Ui {
class FleetControl;
}

struct WaypointElement {
    uint16_t seq;
    uint16_t frame;
    uint16_t command;
    uint16_t type; // 0 mission waypoint, 1 entrance, 2 exit
    uint16_t current;
    uint16_t autocontinue;
    float param1;
    float param2;
    float param3;
    float param4;
    float x;
    float y;
    float z;
};

class FleetControl : public QWidget
{
    Q_OBJECT
public:
    explicit FleetControl(QString address, int port, QString broadcast, int bcport, int sysid, QWidget *parent = 0);
    ~FleetControl();
    //void quitHandler(int sig);

#ifdef NET_CONNECT
    QUrl _urlRedirectedTo;
    QNetworkRequest* request = new QNetworkRequest(QUrl("https://imatisse.hds.utc.fr/fly2/insertDronePos.php"));
    //QNetworkRequest request;
    QNetworkAccessManager *net_manager = new QNetworkAccessManager(this);
#endif

    // setpoint values are stored here
    float targetX, targetY, targetZ;
    float targetHeading;
    float targetPerimeter; // perimeter for the ring formation
    int target_id;

    // formation
    FormationParams formation;
    uint8_t current_formation;

    // geodetic converter
    GeodeticConverter geoConv;

    //float setpointHold;
    // received UAV position values
    float recv_x, recv_y, recv_z;

    uint8_t system_status;

    uint8_t uav_request_id;
    uint8_t elected_id;
    uint8_t replacement_id;
    uint8_t id_to_replace;

    uint8_t active_uav; // system id of the active uav
    bool active_uav_set;

    uint8_t target_system_id;
    uint8_t target_comp_id = 0;

    QString waypointFileName;
    std::vector<WaypointElement> waypoints;

    int64_t heartbeat_period;
    uint8_t heartbeat_uav_type;
    uint8_t uav_autopilot;
    uint8_t uav_base_mode;
    uint32_t uav_custom_mode;
    //uint8_t uav_system_status;

    bool sendingPosition;
    uint64_t sendingPositionInterval;
    bool sendingHeartbeat;
    uint64_t sendingHeartbeatInterval;

private:
    Ui::FleetControl *ui;
    MavlinkServer mavCom;
    MavlinkServer *mavlinkQuit;

    QTimer *timerSendTarget;
    QTimer *timerUpdateNeighTable;
    QTimer *timerInfoUAV;
    QTimer *timerReplacementArrived;
    QTimer *dataTimer;
    QTimer *timerElectionResponse;
    QTimer *timerReplacementResponse;

    std::vector<NeighUAV> localNeighTable;

public slots:
    // from ScenZeroGCS
    void missionStartWaypoint();
    void missionEndWaypoint();
    void clearAllWPs();
    void returnToLaunch();
    void emergencyLand();
    void sendWaypoints(bool uav_set, uint8_t uav_id);
    void pauseContinue();
    void initialElect();
    void replacementRequest();

    void RefreshNeighTable();
    void clearTable();
    void checkReplacement();

    void realtimeDataSlot();

    void handleElection();
    void handleReplacement();

    // waypoints
    void loadWaypointFile();
    //void saveWaypoints();
    //int waypointFileNameChanged(const QString &);

    //void callbackUploadInfo();
    void StartMission();
    void StopMission();
    void SetTarget();
    void fleetTakeOff();
    void fleetLand();

    void SetRingFormation();
    void SetLineFormation();
    void SetColumnFormation();
    void SetStackFormation();

    void callbackSendTarget();
    void callbackUpdateNeighTable();
    void callbackHeartbeat();
    void callbackInfoUAV();

    void UpdateTarget();

    void CalculateFormationPositions(uint8_t formation, float targ_x, float targ_y, float alt, float perimeter, float heading, uint8_t num_drones);

#ifdef NET_CONNECT
    // network
    void onfinish(QNetworkReply *reply);
    QUrl redirectUrl(const QUrl& possibleRedirectUrl, const QUrl& oldRedirectUrl) const;
#endif

    double to_rad(double);
};

#endif // MISSIONPLANNER_H
