//  created:    03/04/2016
//  updated:    07/02/2017
//  filename:   FleetControl.cpp
//
//  author:     Milan Erdelj
//
//  version:    $Id: $
//
//  purpose:    Fleet control Qt application
//
//
/*********************************************************************/

#include "FleetControl.h"
#include "ui_fleetcontrol.h"
#include <cmath>

using namespace std;

FleetControl::FleetControl(QString address, int port, QString broadcast, int bcport, int sysid, QWidget *parent) : QWidget(parent), ui(new Ui::FleetControl), mavCom(address.toStdString(),port,broadcast.toStdString(),bcport, sysid) {
    ui->setupUi(this);

    QLocale::setDefault(QLocale::c());

    connect(ui->btnSetTarget, SIGNAL(clicked()), this, SLOT(SetTarget()));
    connect(ui->btnStartMission, SIGNAL(clicked()), this, SLOT(StartMission()));
    connect(ui->btnStopMission, SIGNAL(clicked()), this, SLOT(StopMission()));
    connect(ui->btnTakeOff, SIGNAL(clicked()), this, SLOT(fleetTakeOff()));
    connect(ui->btnLand, SIGNAL(clicked()), this, SLOT(fleetLand()));
    connect(ui->btnRingForm, SIGNAL(clicked()), this, SLOT(SetRingFormation()));
    connect(ui->btnLineForm, SIGNAL(clicked()), this, SLOT(SetLineFormation()));
    connect(ui->btnStackForm, SIGNAL(clicked()), this, SLOT(SetStackFormation()));
    connect(ui->btnColumnForm, SIGNAL(clicked()), this, SLOT(SetColumnFormation()));
    // scen zero
    connect(ui->btnStart, SIGNAL(clicked()), this, SLOT(missionStartWaypoint()));
    connect(ui->btnEnd, SIGNAL(clicked()), this, SLOT(missionEndWaypoint()));
    connect(ui->btnReplace, SIGNAL(clicked()), this, SLOT(replacementRequest()));
    connect(ui->btnHold, SIGNAL(clicked()), this, SLOT(pauseContinue()));
    connect(ui->btnEmergencyLand, SIGNAL(clicked()), this, SLOT(emergencyLand()));
    connect(ui->btnElect, SIGNAL(clicked()), this, SLOT(initialElect()));
    connect(ui->btnLoadFile, SIGNAL(clicked()), this, SLOT(loadWaypointFile()));
    //connect(ui->btnSaveFile, SIGNAL(clicked()), this, SLOT(saveWaypoints()));
    //connect(ui->editFileName, SIGNAL(textChanged(const QString &)), this, SLOT(waypointFileNameChanged(const QString &)));

#ifdef NET_CONNECT
    connect(net_manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(onfinish(QNetworkReply*)));
    // geo conv
    geoConv.initialiseReference(49.402214, 2.795260, 5);
#endif

    // table view init
    ui->tblUAVs->setColumnCount(COLUMN_COUNT);
    ui->tblUAVs->setRowCount(ROW_COUNT);
    ui->tblUAVs->verticalHeader()->setStyleSheet("color: black");
    ui->tblUAVs->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tblUAVs->verticalHeader()->setVisible(false);
    ui->tblUAVs->horizontalHeader()->setStretchLastSection(true);
    QStringList header_labels;
    header_labels<<"Sys ID"<<"State"<<"X"<<"Y"<<"Z"<<"Lat"<<"Long"<<"Battery";
    for(unsigned int i = 0; i<ROW_COUNT; i++) {
        for(unsigned int j = 0; j<COLUMN_COUNT; j++) {
            ui->tblUAVs->setItem(i, j, new QTableWidgetItem(""));
            ui->tblUAVs->item(i,j)->setTextAlignment(Qt::AlignCenter);
        }
    }
    ui->tblUAVs->setHorizontalHeaderLabels(header_labels);
    clearTable();

    // setpoint values initialization
    targetX = targetY = 0.0;
    targetZ = 1.0;
    targetPerimeter = 4.0;
    target_id = 0;
    targetHeading = 0.0;
    ui->spinX->setValue(targetX);
    ui->spinY->setValue(targetY);
    ui->spinHeading->setValue(targetHeading);
    ui->spinPerimeter->setValue(targetPerimeter);

    target_system_id = 255;
    target_comp_id = 255;

    uav_request_id = 0;
    active_uav_set = false;

    heartbeat_period = HEARTBEAT_DEFAULT_PERIOD;

    // heartbeat flags
    heartbeat_uav_type = MAV_TYPE_GCS;
    uav_autopilot = MAV_AUTOPILOT_GENERIC;
    uav_base_mode = MAV_MODE_FLAG_SAFETY_ARMED;
    uav_custom_mode = 0;
    system_status = MAV_STATE_UNINIT;

    // period function flags
    sendingPosition = false;
    sendingHeartbeat = true;
    sendingPositionInterval = POSITION_DEFAULT_PERIOD;

    // set the uav mode to standby
    system_status = MAV_STATE_STANDBY;

    timerSendTarget = new QTimer(this);
    connect(timerSendTarget, SIGNAL(timeout()), this, SLOT(callbackSendTarget()));
    timerUpdateNeighTable = new QTimer(this);
    connect(timerUpdateNeighTable, SIGNAL(timeout()), this, SLOT(RefreshNeighTable()));
    timerInfoUAV = new QTimer(this);
    connect(timerInfoUAV, SIGNAL(timeout()), this, SLOT(callbackInfoUAV()));
    // timer check replacement
    timerReplacementArrived = new QTimer(this);
    connect(timerReplacementArrived, SIGNAL(timeout()), this, SLOT(checkReplacement()));

    // election an replacement timers
    timerElectionResponse = new QTimer(this);
    connect(timerElectionResponse, SIGNAL(timeout()), this, SLOT(handleElection()));
    timerReplacementResponse = new QTimer(this);
    connect(timerReplacementResponse, SIGNAL(timeout()), this, SLOT(handleReplacement()));

    timerInfoUAV->start(330); // milliseconds
    timerUpdateNeighTable->start(200);
    timerReplacementArrived->start(500);

    ui->btnStart->setEnabled(false);
    ui->btnReplace->setEnabled(false);
    ui->btnHold->setEnabled(false);

    current_formation = FLEET_FORMATION_RING;

    // signal plot setup
    ui->signalPlot->addGraph(); // blue line
    ui->signalPlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->signalPlot->addGraph(); // referent signal
    ui->signalPlot->graph(1)->setPen(QPen(QColor(110, 255, 40)));

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%h:%m:%s");
    ui->signalPlot->xAxis->setTicker(timeTicker);
    ui->signalPlot->axisRect()->setupFullAxesBox();
    ui->signalPlot->yAxis->setRange(-1.1, 1.1);

    ui->voltagePlot->xAxis->setTicker(timeTicker);
    ui->voltagePlot->axisRect()->setupFullAxesBox();
    ui->voltagePlot->yAxis->setRange(11.4, 12.6);

    // make left and bottom axes transfer their ranges to right and top axes:
    connect(ui->signalPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->signalPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->signalPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->signalPlot->yAxis2, SLOT(setRange(QCPRange)));

    // voltage plot setup
    ui->voltagePlot->addGraph(); // blue line
    ui->voltagePlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->voltagePlot->addGraph(); // replacement level
    ui->voltagePlot->graph(1)->setPen(QPen(QColor(110, 255, 40)));
    ui->voltagePlot->addGraph(); // critical level
    ui->voltagePlot->graph(2)->setPen(QPen(QColor(255, 110, 40)));

    // make left and bottom axes transfer their ranges to right and top axes:
    connect(ui->voltagePlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->voltagePlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->voltagePlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->voltagePlot->yAxis2, SLOT(setRange(QCPRange)));

    // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
    //connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));

    dataTimer = new QTimer(this);
    connect(dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
    dataTimer->start(SIGNAL_PERIOD); // Interval 0 means to refresh as fast as possible
}

FleetControl::~FleetControl() {
    delete timerSendTarget;
    delete timerUpdateNeighTable;
    delete timerInfoUAV;
    delete timerReplacementArrived;
    delete dataTimer;
#ifdef NET_CONNECT
    delete net_manager;
    delete request;
#endif
    delete ui;
}

void FleetControl::StartMission() {
    mavCom.cmdFleetStartMission(target_system_id, target_comp_id, 0);
}

void FleetControl::StopMission() {
    mavCom.cmdFleetEndMission(target_system_id, target_comp_id, 0);
    timerSendTarget->stop();
}

void FleetControl::pauseContinue() {
    if(active_uav_set) {
        // send pause/continue depending on the state of the button
        if(ui->btnHold->isChecked()) {
            ui->btnHold->setText("CONTINUE");
            //cout << "[INFO] Sent pause command.\n";
            mavCom.cmdDoPauseContinue(active_uav, 0, 0);
            mavCom.waitCommandAck(ACK_TIMEOUT);
        } else {
            ui->btnHold->setText("HOLD");
            //cout << "[INFO] Sent continue command.\n";
            mavCom.cmdDoPauseContinue(active_uav, 0, 1);
            mavCom.waitCommandAck(ACK_TIMEOUT);
        }
    } else {
        printf("[ERRR] Active UAV not set!\n");
    }
}

void FleetControl::missionStartWaypoint() {
    if(active_uav_set) {
        // start mission
        // send waypoints
        sendWaypoints(active_uav_set, active_uav);

        // set auto armed mode
        //mavCom.cmdDoSetMode(active_uav, 0, MAV_MODE_AUTO_ARMED);
        mavCom.cmdDoSetModeBroadcast(active_uav, 0, MAV_MODE_AUTO_ARMED);
        mavCom.waitCommandAck(ACK_TIMEOUT);

        usleep(INTER_COMMAND_PAUSE);

        // mission start message
        //mavCom.cmdMissionStart(active_uav, 0, mavCom.missionFirst, mavCom.missionLast);
        mavCom.cmdMissionStartBroadcast(active_uav, 0, mavCom.missionFirst, mavCom.missionLast);
        mavCom.waitCommandAck(ACK_TIMEOUT);

        mavCom.missionStarted();
        //ui->btnHold->setEnabled(true);

        mavCom.started_receiving_time = mavCom.get_time_usec();

        ui->btnHold->setEnabled(true);
    } else {
        printf("[ERRR] Active UAV not set!\n");
    }
}

void FleetControl::missionEndWaypoint() {
    clearAllWPs();
    // return to launch
    //mavCom.cmdNavReturnToLaunch(active_uav, 0);
    mavCom.cmdNavReturnToLaunchBroadcast(target_system_id, 0);
    mavCom.waitCommandAck(ACK_TIMEOUT);
    //stopSendSetpoint();
    ui->btnStart->setText("START MISSION");
    ui->btnHold->setEnabled(false);
}

void FleetControl::returnToLaunch() {
    //cout << "[INFO] RETURN TO LAUNCH\n";
    if(active_uav_set) {
        //mavCom.cmdNavReturnToLaunch(active_uav, 0);
        mavCom.cmdNavReturnToLaunchBroadcast(active_uav, 0);
        mavCom.waitCommandAck(ACK_TIMEOUT);
        //stopSendSetpoint();
    } else {
        printf("[ERRR] Active UAV not set!\n");
    }
}

void FleetControl::clearAllWPs() {
    // message clear all
    if(active_uav_set) {
        mavCom.sendMissionClearAllBroadcast(active_uav, 0);
        //mavCom.sendMissionClearAll(active_uav, 0);
        mavCom.waitMissionAck(ACK_TIMEOUT);
        //cout << "[INFO] All waypoints cleared.\n";
    } else {
        printf("[ERRR] Active UAV not set!\n");
    }
}

void FleetControl::fleetTakeOff() {
    //mavlinkUDP.cmdNavTakeoff(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    mavCom.cmdNavTakeoffBroadcast(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    // ACK will be implemented when sending unicast to everyone
    //mavlinkUDP.waitCommandAck(ACK_TIMEOUT);
}

void FleetControl::fleetLand() {
    //mavlinkUDP.cmdNavLand(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    mavCom.cmdNavLandBroadcast(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    // ACK will be implemented when sending unicast to everyone
    //mavlinkUDP.waitCommandAck(ACK_TIMEOUT);
    timerSendTarget->stop();
}

void FleetControl::UpdateTarget() {
    // change the values
    targetX = ui->spinX->value();
    targetY = ui->spinY->value();
    targetHeading = ui->spinHeading->value();
    targetPerimeter = ui->spinPerimeter->value();
}

void FleetControl::SetTarget() {
    UpdateTarget();
    mavCom.sendFleetSetTargetLocal(target_id, current_formation, targetX, targetY, targetZ, (int16_t)targetHeading, 0, 0, targetPerimeter);
    //    // for different target types
    //    switch(ui->comboTargetType->currentIndex()) {
    //    case 0:
    //        // undefined
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_UNDEFINED, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    case 1:
    //        // point
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_POINT, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    case 2:
    //        // structure
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_STRUCTURE, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    case 3:
    //        // crowd
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_CROWD, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    case 4:
    //        // emergency
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_EMERGENCY, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    case 5:
    //        // delivery
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_DELIVERY, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    case 6:
    //        // barrier
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_BARRIER, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    case 7:
    //        // other
    //        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_OTHER, targetX, targetY, targetZ, 0, 0, 0, targetR);
    //        break;
    //    default:
    //        break;
    //    }
}

void FleetControl::SetRingFormation() {
    current_formation = FLEET_FORMATION_RING;
    SetTarget();
    //mavCom.sendFleetSetTargetLocal(target_id, FLEET_FORMATION_RING, targetX, targetY, targetZ, 0, 0, 0, targetR);
}

void FleetControl::SetStackFormation() {
    current_formation = FLEET_FORMATION_STACK;
    SetTarget();
    //mavCom.sendFleetSetTargetLocal(target_id, FLEET_FORMATION_STACK, targetX, targetY, targetZ, 0, 0, 0, targetR);
}

void FleetControl::SetColumnFormation() {
    current_formation = FLEET_FORMATION_COLUMN;
    SetTarget();
    //mavCom.sendFleetSetTargetLocal(target_id, FLEET_FORMATION_COLUMN, targetX, targetY, targetZ, 0, 0, 0, targetR);
}

void FleetControl::SetLineFormation() {
    current_formation = FLEET_FORMATION_LINE;
    SetTarget();
    //mavCom.sendFleetSetTargetLocal(target_id, FLEET_FORMATION_LINE, targetX, targetY, targetZ, 0, 0, 0, targetR);
}

void FleetControl::CalculateFormationPositions(uint8_t formation, float targ_x, float targ_y, float alt, float perimeter, float heading, uint8_t num_drones)
{
    float x ,y, z;
    for(int i=0; i<num_drones; i++) {
        switch(formation) {
        case FLEET_FORMATION_RING:
            x = targ_x + perimeter*cos(i*to_rad(360)/num_drones + to_rad(heading))/2;
            y = targ_y + perimeter*sin(i*to_rad(360)/num_drones + to_rad(heading))/2;
            z = alt;
            break;
        case FLEET_FORMATION_LINE:
            x = targ_x + perimeter*(i/((float)num_drones-1)-0.5)*cos(to_rad(heading-90));
            y = targ_y + perimeter*(i/((float)num_drones-1)-0.5)*sin(to_rad(heading-90));
            z = alt;
            break;
        case FLEET_FORMATION_COLUMN:
            x = targ_x + perimeter*(i/((float)num_drones-1)-0.5)*cos(to_rad(heading));
            y = targ_y + perimeter*(i/((float)num_drones-1)-0.5)*sin(to_rad(heading));
            z = alt;
            break;
        case FLEET_FORMATION_STACK:
            x = targ_x;
            y = targ_y;
            z = alt + i*(perimeter/((float)num_drones-1));
            break;
        default:
            break;
        }
        printf("[FORMATION %d] ID %d X %.2f Y %.2f Z %.2f\n", formation, i, x, y, z);
    }
}

void FleetControl::loadWaypointFile() {
    waypointFileName = QFileDialog::getOpenFileName(this, "Open MAVLink Waypoint File", "", "MAVLink Waypoint Files (*.wpt);;All Files (*)");
    if(waypointFileName.isEmpty())
        return;
    else {
        ui->editFileName->setText(waypointFileName);
        // read the file
        QFile file(waypointFileName);
        QString line;
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox::information(this, "Unable to open file", file.errorString());
            return;
        } else {
            // read the contents
            QTextStream textStream(&file);
            waypoints.clear();
            while(!textStream.atEnd()) {
                line = textStream.readLine();
                //qDebug() << "Line: " << line;
                if(line.startsWith('#',Qt::CaseSensitive) || line.startsWith('<',Qt::CaseSensitive)) {
                    //cout << "New line: Comment.\n";
                } else {
                    try {
                        QTextStream lineStream(&line);
                        WaypointElement tempElem;
                        lineStream >> tempElem.seq;
                        lineStream >> tempElem.frame;
                        lineStream >> tempElem.command;
                        lineStream >> tempElem.type;
                        lineStream >> tempElem.current;
                        lineStream >> tempElem.autocontinue;
                        lineStream >> tempElem.param1;
                        lineStream >> tempElem.param2;
                        lineStream >> tempElem.param3;
                        lineStream >> tempElem.param4;
                        lineStream >> tempElem.x;
                        lineStream >> tempElem.y;
                        lineStream >> tempElem.z;
                        waypoints.push_back(tempElem);
                        cout << "seq " << tempElem.seq << " frame " << tempElem.frame << " command " << tempElem.command << " type " << tempElem.type << " current " << tempElem.current << " autocont " << tempElem.autocontinue << " param1 " << tempElem.param1 << " param2 " << tempElem.param2 << " param3 " << tempElem.param3 << " param4 " << tempElem.param4 << " x " << tempElem.x << " y " << tempElem.y << " z " << tempElem.z << endl;
                    } catch(...) {
                        QMessageBox::warning(this, "Parsing error", "File corrupted!");
                    }
                }
            }
        }
        file.close();
    }
}

void FleetControl::sendWaypoints(bool uav_set, uint8_t uav_id) {
    if(uav_set) {
        // reset all the previous waypoints
        clearAllWPs();
        printf("in sendWaypoints.\n");
        // send all the waypoint vector elements
        for(std::vector<WaypointElement>::iterator iter = waypoints.begin(); iter != waypoints.end(); ++iter) {
            //mavlinkUDP.sendMissionItem(target_system_id, target_comp_id, 7, MAV_FRAME_LOCAL_NED, MAV_CMD_NAV_WAYPOINT, 0, 1, ui->wp3hold->value(), 0, 0, ui->wp3yaw->value(), 10, 10, 15);
            //mavCom.sendMissionItem(uav_id, 0, iter->seq, (uint8_t)iter->frame, iter->command, (uint8_t)iter->current, (uint8_t)iter->autocontinue, iter->param1, iter->param2, iter->param3, iter->param4, iter->x, iter->y, iter->z);
            //printf("Waypoint.\n");
            mavCom.sendMissionItemBroadcast(uav_id, 0, iter->seq, (uint8_t)iter->frame, iter->command, (uint8_t)iter->current, (uint8_t)iter->autocontinue, iter->param1, iter->param2, iter->param3, iter->param4, iter->x, iter->y, iter->z);
            mavCom.waitMissionAck(ACK_TIMEOUT);
            //usleep(INTER_COMMAND_PAUSE);
        }
    } else {
        printf("[ERRR] Active UAV not set!\n");
    }
}


void FleetControl::clearTable() {
    for(unsigned int i = 0; i<ROW_COUNT; i++) {
        for(unsigned int j = 0; j<COLUMN_COUNT; j++) {
            ui->tblUAVs->item(i,j)->setText("");
        }
    }
}

void FleetControl::RefreshNeighTable() {
    uint8_t num_elements = 0;
    uint64_t latency;
    std::set<uint8_t>::iterator it;
    // remove old entries
    it = mavCom.activeUAVs.begin();
    for(; it != mavCom.activeUAVs.end(); ) {
        latency = mavCom.get_time_usec() - mavCom.neighUAVs[*it].last_update;
        if(latency > UAV_INFO_TIMEOUT) {
            // exclude the drone
            mavCom.activeUAVs.erase(it++);
        } else {
            // keep the drone info
            num_elements++;
            ++it;
        }
    }
    clearTable();
    // populate the tablewidget
    uint8_t count = 0;
    for(it = mavCom.activeUAVs.begin(); it != mavCom.activeUAVs.end(); ++it) {
        uint8_t current_id = *it;
        //printf("ACTIVE_UAVS: id=%d=%d x=%.2f y=%.2f z=%.2f custom_state=%d\n", current_id, mavlinkUDP.infoUAV[current_id].system_id, mavlinkUDP.infoUAV[current_id].x, mavlinkUDP.infoUAV[current_id].y, mavlinkUDP.infoUAV[current_id].z, mavlinkUDP.infoUAV[current_id].custom_mode);
        // sys ID
        ui->tblUAVs->item(count,0)->setText(QString::number(current_id));
        // state
        switch(mavCom.neighUAVs[current_id].custom_mode) {
        case HANDOVER_STATE_INIT:
            // HANDOVER_STATE_INIT
            ui->tblUAVs->item(count,1)->setText("INIT");
            break;
        case HANDOVER_STATE_STANDBY:
            // HANDOVER_STATE_STANDBY
            ui->tblUAVs->item(count,1)->setText("STANDBY");
            break;
        case HANDOVER_STATE_MISSION_OFFER:
            // HANDOVER_STATE_MISSION_OFFER
            ui->tblUAVs->item(count,1)->setText("MISSION_OFFER");
            break;
        case HANDOVER_STATE_MISSION_EXECUTION:
            // HANDOVER_STATE_MISSION_EXECUTION
            ui->tblUAVs->item(count,1)->setText("MISSION_EXEC");
            break;
        case HANDOVER_STATE_REPLACEMENT_OFFER:
            // HANDOVER_STATE_REPLACEMENT_OFFER
            ui->tblUAVs->item(count,1)->setText("REPLACEMENT_OFFER");
            break;
        case HANDOVER_STATE_POSITIONING:
            // HANDOVER_STATE_POSITIONING
            ui->tblUAVs->item(count,1)->setText("POSITIONING");
            break;
        case HANDOVER_STATE_REPLACEMENT_REQUEST:
            // HANDOVER_STATE_REPLACEMENT_REQUEST
            ui->tblUAVs->item(count,1)->setText("REPLACEMENT_REQUEST");
            break;
        case HANDOVER_STATE_RECHARGING:
            // HANDOVER_STATE_RECHARGING
            ui->tblUAVs->item(count,1)->setText("RECHARGING");
            break;
        default:
            break;
        }
        // x, y, z
        ui->tblUAVs->item(count,2)->setText(QString::number(mavCom.neighUAVs[current_id].x));
        ui->tblUAVs->item(count,3)->setText(QString::number(mavCom.neighUAVs[current_id].y));
        ui->tblUAVs->item(count,4)->setText(QString::number(mavCom.neighUAVs[current_id].z));
        // lat, long
        double latitude, longitude, altitude;
        //printf("ID %d LAT %lf LONG %lf\n", current_id, latitude, longitude);
        geoConv.ned2Geodetic(mavCom.neighUAVs[current_id].x, mavCom.neighUAVs[current_id].y,mavCom.neighUAVs[current_id].z, &latitude, &longitude, &altitude);
        ui->tblUAVs->item(count,5)->setText(QString::number(latitude));
        ui->tblUAVs->item(count,6)->setText(QString::number(longitude));
        mavCom.neighUAVs[current_id].latitude = (uint32_t)(latitude*10000000);
        mavCom.neighUAVs[current_id].longitude = (uint32_t)(longitude*10000000);
        mavCom.neighUAVs[current_id].altitude = (uint32_t)altitude;
        // battery
        ui->tblUAVs->item(count,7)->setText(QString::number(mavCom.neighUAVs[current_id].voltage_battery));

#ifdef NET_CONNECT
        // JSON object
        QJsonObject json;
        json.insert("system_id",mavCom.neighUAVs[current_id].system_id);
        QString address = "192.168.6." + QString::number(mavCom.neighUAVs[current_id].system_id);
        json.insert("ip_address",address);
        json.insert("latitude",mavCom.neighUAVs[current_id].latitude);
        json.insert("longitude",mavCom.neighUAVs[current_id].longitude);
        json.insert("altitude",mavCom.neighUAVs[current_id].altitude);
        json.insert("relative_alt",mavCom.neighUAVs[current_id].relative_alt);
        json.insert("heading",mavCom.neighUAVs[current_id].heading);
        json.insert("type",mavCom.neighUAVs[current_id].type);
        json.insert("autopilot",mavCom.neighUAVs[current_id].autopilot);
        json.insert("base_mode",mavCom.neighUAVs[current_id].base_mode);
        json.insert("custom_mode",mavCom.neighUAVs[current_id].custom_mode);
        json.insert("system_status",mavCom.neighUAVs[current_id].system_status);
        json.insert("voltage_battery",mavCom.neighUAVs[current_id].voltage_battery);
        json.insert("drop_rate_comm",mavCom.neighUAVs[current_id].drop_rate_comm);
        json.insert("errors_comm",mavCom.neighUAVs[current_id].errors_comm);
        json.insert("vx",mavCom.neighUAVs[current_id].vx);
        json.insert("vy",mavCom.neighUAVs[current_id].vy);
        json.insert("vz",mavCom.neighUAVs[current_id].vz);
        json.insert("sensors_present",(uint8_t)mavCom.neighUAVs[current_id].sensors_present);
        json.insert("sensors_enabled",(uint8_t)mavCom.neighUAVs[current_id].sensors_enabled);
        json.insert("sensors_health",(uint8_t)mavCom.neighUAVs[current_id].sensors_health);
        json.insert("mainloop_load",mavCom.neighUAVs[current_id].mainloop_load);
        json.insert("current_battery",mavCom.neighUAVs[current_id].current_battery);
        json.insert("battery_remaining",mavCom.neighUAVs[current_id].battery_remaining);

        QJsonDocument json_doc(json);
        QByteArray json_bytes = json_doc.toJson();
        QByteArray postDataSize = QByteArray::number(json_bytes.size());

        // Add the headers specifying their names and their values
        request->setRawHeader("User-Agent", "StationFleet");
        request->setRawHeader("X-Custom-User-Agent", "StationFleet");
        request->setRawHeader("Content-Type", "application/x-www-form-urlencoded");
        request->setRawHeader("Content-Length", postDataSize);
        request->setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        // upload request
        net_manager->post(*request,json_bytes);
#endif
        count++;
    }
    // tablewidget has to be refreshed
    ui->tblUAVs->viewport()->update();
}

void FleetControl::callbackHeartbeat() {
    mavCom.sendHeartbeat(heartbeat_uav_type, uav_autopilot, uav_base_mode, uav_custom_mode, system_status);
}

void FleetControl::callbackInfoUAV() {
    // sending HEARTBEAT, SYS_STATUS and LOCAL_POSITION_NED
    mavCom.sendInfoUAV(heartbeat_uav_type, uav_autopilot, uav_base_mode, uav_custom_mode, system_status, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    // sending custom info message
    //mavlinkUDP.sendFleetUavInfoLocal(uav_base_mode, uav_custom_mode, system_status, 0, 0, 0, 0, 0, 0, 0, 0);
}

void FleetControl::callbackUpdateNeighTable() {
    RefreshNeighTable();
    checkReplacement();
}

void FleetControl::callbackSendTarget() {
    SetTarget();
}

void FleetControl::emergencyLand() {
    mavCom.cmdNavLandBroadcast(255, 0, 0, 0, 0, 0, 0);
    mavCom.cmdNavLandBroadcast(255, 0, 0, 0, 0, 0, 0);
    mavCom.cmdNavLandBroadcast(255, 0, 0, 0, 0, 0, 0);
    //mavCom.waitCommandAck(ACK_TIMEOUT);
}

void FleetControl::initialElect() {
    // initial election request
    mavCom.sendFleetUavRequestLocal(REQUEST_TYPE_INITIAL, ++uav_request_id, id_to_replace, 0, 0, 0, 0);
    // wait for the responses for 1 second
    timerElectionResponse->start(1000); // timeout after 1 sec
}

void FleetControl::handleElection() {
    uint64_t min_arrival;
    uint8_t min_arrival_id;
    uint64_t curr_time;

    // stop the timer
    timerElectionResponse->stop();

    // handle the election results
    if(mavCom.reqResponses.size() > 0) {
        min_arrival = mavCom.reqResponses.at(0).arrival_time;
        min_arrival_id = mavCom.reqResponses.at(0).system_id;
    }
    for(unsigned int i=1; i<mavCom.reqResponses.size(); i++) {
        if(mavCom.reqResponses.at(i).request_id == uav_request_id) {
            if(mavCom.reqResponses.at(i).arrival_time < min_arrival) {
                min_arrival = mavCom.reqResponses.at(i).arrival_time;
                min_arrival_id = mavCom.reqResponses.at(i).system_id;
            }
        }
    }
    // clear the responses
    while(!mavCom.reqResponses.empty())
        mavCom.reqResponses.pop_back();
    // publish the elected id
    mavCom.sendFleetUavElectedID(uav_request_id, min_arrival_id);
    //mavCom.sendFleetUavElectedID(uav_request_id, min_arrival_id);
    //mavCom.sendFleetUavElectedID(uav_request_id, min_arrival_id);
    //wait for response accept until timeout
    curr_time = mavCom.get_time_usec();
    while(mavCom.get_time_usec()<(curr_time + PERIOD_SECOND)) {
        // wait for the response
        if(mavCom.req_accepted) break;
        usleep(1000);
    }
    if(mavCom.req_accepted) {
        // OK
        active_uav = elected_id = min_arrival_id;
        active_uav_set = true;
        printf("[INFO] Elected ID: %d\n", elected_id);
        QString label = "SYSID " + QString::number(active_uav);
        ui->lblActiveUAV->setText(label);
        mavCom.req_accepted = false; // put it back to false for the next time
        ui->btnReplace->setEnabled(true);
        ui->btnStart->setEnabled(true);
    } else {
        // not OK
        active_uav_set = false;
        ui->lblActiveUAV->setText("Not set!");
        printf("[ERRR] Initial election failed!\n");
        ui->btnReplace->setEnabled(false);
        ui->btnStart->setEnabled(false);
    }
}

void FleetControl::checkReplacement() {
    if(mavCom.replacement_in_position) {
        // replacement arrived
        ui->lblReplacement->setText("ARRIVED");
        // replaced uav should return
        printf("[DEBUG] current active UAV %d\n", active_uav);
        mavCom.cmdNavReturnToLaunch(active_uav, 0);
        //mavCom.cmdNavReturnToLaunchBroadcast(active_uav, 0);
        mavCom.cmdNavReturnToLaunch(active_uav, 0);
        mavCom.cmdNavReturnToLaunch(active_uav, 0);
        // transfer mission items
        sendWaypoints(active_uav_set, replacement_id);
        // exchange signal value:
        mavCom.cmdDoSetParameter(replacement_id, 0, HANDOVER_PARAM, (float)mavCom.recv_signal);
        //mavCom.cmdMissionStart(replacement_id, 0, mavCom.missionFirst, mavCom.missionLast);
        mavCom.cmdMissionStartBroadcast(replacement_id, 0, mavCom.missionFirst, mavCom.missionLast);
        mavCom.waitCommandAck(ACK_TIMEOUT);
        //mavCom.sendMissionSetCurrent(replacement_id, 0, mavCom.seqr); // current mission item
        //mavCom.sendMissionSetCurrent(replacement_id, 0, mavCom.currentMissionItem); // current mission item
        mavCom.sendMissionSetCurrentBroadcast(replacement_id, 0, mavCom.currentMissionItem); // current mission item
        printf("[DEBUG] seqr %d\n", mavCom.seqr);
        printf("[DEBUG] currentMissionItem %d\n", mavCom.currentMissionItem);
        active_uav = replacement_id;
        printf("[DEBUG] new active UAV %d\n", active_uav);
        active_uav_set = true;
        QString label = "SYSID " + QString::number(active_uav);
        ui->lblActiveUAV->setText(label);
        ui->lblReplacement->setText("");
        //        // publish the active UAV?
        //        mavlinkUDP.sendFleetUavElectedID(uav_request_id, active_uav);
        mavCom.replacement_in_position = false;
    }
}

void FleetControl::replacementRequest() {
    if(active_uav_set) {
        // replacement request for active UAV
        mavCom.sendFleetUavRequestLocal(REQUEST_TYPE_REPLACEMENT, ++uav_request_id, active_uav, mavCom.neighUAVs[active_uav].x, mavCom.neighUAVs[active_uav].y, mavCom.neighUAVs[active_uav].z, 0);
        // wait for the responses for 1 second
        timerReplacementResponse->start(1000);
    } else {
        printf("[ERRR] Active UAV not set!\n");
    }
}

void FleetControl::handleReplacement() {
    uint64_t min_arrival;
    uint8_t min_arrival_id;
    uint64_t curr_time;

    // stop the timer
    timerReplacementResponse->stop();

    // handle the replacement results
    if(mavCom.reqResponses.size() > 0) {
        min_arrival = mavCom.reqResponses.at(0).arrival_time;
        min_arrival_id = mavCom.reqResponses.at(0).system_id;
    }
    for(unsigned int i=1; i<mavCom.reqResponses.size(); i++) {
        if(mavCom.reqResponses.at(i).request_id == uav_request_id) {
            if(mavCom.reqResponses.at(i).arrival_time < min_arrival) {
                min_arrival = mavCom.reqResponses.at(i).arrival_time;
                min_arrival_id = mavCom.reqResponses.at(i).system_id;
            }
        }
    }
    // clear the responses
    while(!mavCom.reqResponses.empty())
        mavCom.reqResponses.pop_back();
    // publish the elected id
    mavCom.sendFleetUavElectedID(uav_request_id, min_arrival_id);
    //mavCom.sendFleetUavElectedID(uav_request_id, min_arrival_id);
    //mavCom.sendFleetUavElectedID(uav_request_id, min_arrival_id);
    //wait for response accept until timeout
    curr_time = mavCom.get_time_usec();
    while(mavCom.get_time_usec()<(curr_time + PERIOD_SECOND)) {
        // wait for the response
        if(mavCom.req_accepted) break;
        usleep(1000);
    }
    if(mavCom.req_accepted) {
        // OK
        replacement_id = min_arrival_id;
        printf("[INFO] Replacement ID: %d\n", replacement_id);
        // follow the currently active UAV
        mavCom.cmdDoFollow(replacement_id, 0, active_uav);
        QString lblTemp;
        lblTemp = "(replacement ID " + QString::number(replacement_id) + ")";
        ui->lblReplacement->setText(lblTemp);
        mavCom.req_accepted = false; // put it back to false for the next time
    } else {
        // not OK
        ui->lblActiveUAV->setText("");
        printf("[ERRR] Replacement election failed!\n");
    }
}

void FleetControl::realtimeDataSlot()
{
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    static double lastPointKey = 0;
    if (key-lastPointKey > 0.02) // at most add point every 20 ms
    {
        // add data to lines:
        ui->signalPlot->graph(0)->addData(key, qSin(((double)mavCom.recv_signal)/SIGNAL_DIVISOR));
        //ui->signalPlot->graph(1)->addData(key, qSin(((double)mavCom.referent_signal)/SIGNAL_DIVISOR));
        ui->signalPlot->graph(1)->addData(key, qSin(((double)(mavCom.get_time_usec() - mavCom.started_receiving_time))/(1000*20*SIGNAL_DIVISOR)));

        ui->voltagePlot->graph(0)->addData(key, (double)mavCom.recv_voltage/100);
        ui->voltagePlot->graph(1)->addData(key, VOLTAGE_REPLACEMENT);
        ui->voltagePlot->graph(2)->addData(key, VOLTAGE_CRITICAL);
        // rescale value (vertical) axis to fit the current data:
        //ui->customPlot->graph(0)->rescaleValueAxis();
        //ui->customPlot->graph(1)->rescaleValueAxis(true);

        // write output
        //printf("GCS_PWR_OUT %.2lf %.2lf %.2lf\n", key, qSin(((double)mavCom.recv_signal)/SIGNAL_DIVISOR), (double)mavCom.recv_voltage/100);

        lastPointKey = key;
    }

    // increment the referent signal
    if(mavCom.started_receiving)
    {
        //printf("REF_SIGNAL %.4lf %.4lf\n", key, qSin(((double)mavCom.recv_signal)/SIGNAL_DIVISOR));
        printf("REF_SIGNAL %.4lf %.4lf\n", key, qSin(((double)(mavCom.get_time_usec() - mavCom.started_receiving_time))/(1000*20*SIGNAL_DIVISOR)));
        printf("MAV_PWR_RECV %.4f %.4f\n", key, qSin(((double)mavCom.referent_signal)/SIGNAL_DIVISOR));
        mavCom.referent_signal++;
    }

    // make key axis range scroll with the data (at a constant range size of 8):
    ui->signalPlot->xAxis->setRange(key, 8, Qt::AlignRight);
    ui->signalPlot->replot();
    ui->voltagePlot->xAxis->setRange(key, 8, Qt::AlignRight);
    ui->voltagePlot->replot();

    //    // calculate frames per second:
    //    static double lastFpsKey;
    //    static int frameCount;
    //    ++frameCount;
    //    if (key-lastFpsKey > 2) // average fps over 2 seconds
    //    {
    //        ui->labelFps->setText(QString("%1 FPS, Total Data points: %2")
    //                           .arg(frameCount/(key-lastFpsKey), 0, 'f', 0)
    //                           .arg(ui->customPlot->graph(0)->data()->size()+ui->customPlot->graph(1)->data()->size()));
    //      lastFpsKey = key;
    //      frameCount = 0;
    //    }
}

#ifdef NET_CONNECT
// connection with the web server
void FleetControl::onfinish(QNetworkReply *reply)
{
    /*
     * Reply is finished!
     * We'll ask for the reply about the Redirection attribute
     * http://doc.trolltech.com/qnetworkrequest.html#Attribute-enum
     */

    //ui->textBox->clear();

    QVariant possibleRedirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

    /* We'll deduct if the redirection is valid in the redirectUrl function */
    _urlRedirectedTo = this->redirectUrl(possibleRedirectUrl.toUrl(),_urlRedirectedTo);

    /* If the URL is not empty, we're being redirected. */
    if(!_urlRedirectedTo.isEmpty()) {
        //QString text = QString("QNAMRedirect::replyFinished: Redirected to ").append(_urlRedirectedTo.toString());
        //ui->textBox->append(text);

        /* We'll do another request to the redirection url. */
        net_manager->get(QNetworkRequest(_urlRedirectedTo));
    }
    else {
        //QString text = QString("QNAMRedirect::replyFinished: Arrived to ").append(reply->url().toString());
        //ui->textBox->append(text);
        _urlRedirectedTo.clear();
    }

    //    /// try to get the data
    //    QByteArray bts = reply->readAll();
    //    QString str(bts);
    //    ui->textBox->append(str);

    /* Clean up. */
    reply->deleteLater();
}

QUrl FleetControl::redirectUrl(const QUrl& possibleRedirectUrl, const QUrl& oldRedirectUrl) const {
    QUrl redirectUrl;
    /*
     * Check if the URL is empty and
     * that we aren't being fooled into a infinite redirect loop.
     * We could also keep track of how many redirects we have been to
     * and set a limit to it, but we'll leave that to you.
     */
    if(!possibleRedirectUrl.isEmpty() &&
            possibleRedirectUrl != oldRedirectUrl) {
        redirectUrl = possibleRedirectUrl;
    }
    return redirectUrl;
}
#endif

double FleetControl::to_rad(double deg)
{
    return deg * M_PI / 180.0;
}
