#include "FleetControl.h"
#include <QApplication>
#include <QCommandLineParser>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("FleetGCS");
    QApplication::setApplicationVersion("2.1");
    QCommandLineParser parser;
    parser.setApplicationDescription("Ground control station app.");
    parser.addHelpOption();
    parser.addVersionOption();
    //    parser.addPositionalArgument("address", QCoreApplication::translate("main", "Drone address."));
    //    parser.addPositionalArgument("port", QCoreApplication::translate("main", "Drone port."));

    //        // A boolean option with a single name (-p)
    //        QCommandLineOption showProgressOption("p", QCoreApplication::translate("main", "Show progress during copy"));
    //        parser.addOption(showProgressOption);

    //        // A boolean option with multiple names (-a, --address)
    //        QCommandLineOption addressOption(QStringList() << "a" << "address", QCoreApplication::translate("main", "IP address of the UAV."));
    //        parser.addOption(addressOption);

    // address option with a value
    QCommandLineOption addressOption(QStringList() << "a" << "address", QCoreApplication::translate("main", "IP address of the UAV."), QCoreApplication::translate("main", "address"));
    addressOption.setDefaultValue("127.0.0.1");
    parser.addOption(addressOption);
    // port option with a value
    QCommandLineOption portOption(QStringList() << "p" << "port", QCoreApplication::translate("main", "Socket port of the UAV."), QCoreApplication::translate("main", "port"));
    portOption.setDefaultValue("5555");
    parser.addOption(portOption);
    // broadcast option with a value
    QCommandLineOption broadcastOption(QStringList() << "b" << "broadcast", QCoreApplication::translate("main", "Broadcast address."), QCoreApplication::translate("main", "broadcast"));
    broadcastOption.setDefaultValue("127.255.255.255");
    parser.addOption(broadcastOption);
    // Broadcast port option with a value
    QCommandLineOption bcportOption(QStringList() << "c" << "bcport", QCoreApplication::translate("main", "Broadcast port."), QCoreApplication::translate("main", "bcport"));
    bcportOption.setDefaultValue("9999");
    parser.addOption(bcportOption);
//    // Boost port option with a value
//    QCommandLineOption boostPortOption(QStringList() << "s" << "boost_port", QCoreApplication::translate("main", "Boost port."), QCoreApplication::translate("main", "boost_port"));
//    boostPortOption.setDefaultValue("1701");
//    parser.addOption(boostPortOption);
    // systemID option with a value
    QCommandLineOption sysidOption(QStringList() << "i" << "sysid", QCoreApplication::translate("main", "System ID of the UAV."), QCoreApplication::translate("main", "sysid"));
    sysidOption.setDefaultValue("150");
    parser.addOption(sysidOption);

    // Process the actual command line arguments given by the user
    parser.process(app);

    const QStringList args = parser.positionalArguments();
    // source is args.at(0), destination is args.at(1)

    QString addr = parser.value(addressOption);
    int port = parser.value(portOption).toInt();
    QString broadcast = parser.value(broadcastOption);
    int sysid = parser.value(sysidOption).toInt();
    int bcport = parser.value(bcportOption).toInt();

    std::cout << "IP address: " << addr.toStdString() << std::endl;
    std::cout << "Port: " << port << std::endl;
    std::cout << "Broadcast: " << broadcast.toStdString() << std::endl;
    std::cout << "Broadcast port: " << bcport << std::endl;
    std::cout << "System ID: " << sysid << std::endl;

    FleetControl w(addr,port,broadcast,bcport,sysid);
    w.show();
    return app.exec();
}
