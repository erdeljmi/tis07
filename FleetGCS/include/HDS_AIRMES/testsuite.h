/** @file
 *    @brief MAVLink comm protocol testsuite generated from HDS_AIRMES.xml
 *    @see http://qgroundcontrol.org/mavlink/
 */
#pragma once
#ifndef HDS_AIRMES_TESTSUITE_H
#define HDS_AIRMES_TESTSUITE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAVLINK_TEST_ALL
#define MAVLINK_TEST_ALL
static void mavlink_test_common(uint8_t, uint8_t, mavlink_message_t *last_msg);
static void mavlink_test_HDS_AIRMES(uint8_t, uint8_t, mavlink_message_t *last_msg);

static void mavlink_test_all(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
    mavlink_test_common(system_id, component_id, last_msg);
    mavlink_test_HDS_AIRMES(system_id, component_id, last_msg);
}
#endif

#include "../common/testsuite.h"


static void mavlink_test_fleet_uav_info_local(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_uav_info_local_t packet_in = {
        963497464,45.0,73.0,101.0,129.0,18275,18379,18483,18587,89,156
    };
    mavlink_fleet_uav_info_local_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.custom_mode = packet_in.custom_mode;
        packet1.x = packet_in.x;
        packet1.y = packet_in.y;
        packet1.z = packet_in.z;
        packet1.heading = packet_in.heading;
        packet1.voltage_battery = packet_in.voltage_battery;
        packet1.battery_remaining = packet_in.battery_remaining;
        packet1.drop_rate_comm = packet_in.drop_rate_comm;
        packet1.errors_drop = packet_in.errors_drop;
        packet1.base_mode = packet_in.base_mode;
        packet1.system_status = packet_in.system_status;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_UAV_INFO_LOCAL_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_local_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_uav_info_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_local_pack(system_id, component_id, &msg , packet1.base_mode , packet1.custom_mode , packet1.system_status , packet1.voltage_battery , packet1.battery_remaining , packet1.drop_rate_comm , packet1.errors_drop , packet1.x , packet1.y , packet1.z , packet1.heading );
    mavlink_msg_fleet_uav_info_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_local_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.base_mode , packet1.custom_mode , packet1.system_status , packet1.voltage_battery , packet1.battery_remaining , packet1.drop_rate_comm , packet1.errors_drop , packet1.x , packet1.y , packet1.z , packet1.heading );
    mavlink_msg_fleet_uav_info_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_uav_info_local_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_local_send(MAVLINK_COMM_1 , packet1.base_mode , packet1.custom_mode , packet1.system_status , packet1.voltage_battery , packet1.battery_remaining , packet1.drop_rate_comm , packet1.errors_drop , packet1.x , packet1.y , packet1.z , packet1.heading );
    mavlink_msg_fleet_uav_info_local_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_uav_info_global(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_uav_info_global_t packet_in = {
        963497464,963497672,963497880,17859,17963,18067,18171,18275,18379,18483,211,22
    };
    mavlink_fleet_uav_info_global_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.custom_mode = packet_in.custom_mode;
        packet1.latitude = packet_in.latitude;
        packet1.longitude = packet_in.longitude;
        packet1.voltage_battery = packet_in.voltage_battery;
        packet1.battery_remaining = packet_in.battery_remaining;
        packet1.drop_rate_comm = packet_in.drop_rate_comm;
        packet1.errors_drop = packet_in.errors_drop;
        packet1.altitude = packet_in.altitude;
        packet1.relative_alt = packet_in.relative_alt;
        packet1.heading = packet_in.heading;
        packet1.base_mode = packet_in.base_mode;
        packet1.system_status = packet_in.system_status;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_UAV_INFO_GLOBAL_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_global_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_uav_info_global_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_global_pack(system_id, component_id, &msg , packet1.base_mode , packet1.custom_mode , packet1.system_status , packet1.voltage_battery , packet1.battery_remaining , packet1.drop_rate_comm , packet1.errors_drop , packet1.latitude , packet1.longitude , packet1.altitude , packet1.relative_alt , packet1.heading );
    mavlink_msg_fleet_uav_info_global_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_global_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.base_mode , packet1.custom_mode , packet1.system_status , packet1.voltage_battery , packet1.battery_remaining , packet1.drop_rate_comm , packet1.errors_drop , packet1.latitude , packet1.longitude , packet1.altitude , packet1.relative_alt , packet1.heading );
    mavlink_msg_fleet_uav_info_global_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_uav_info_global_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_info_global_send(MAVLINK_COMM_1 , packet1.base_mode , packet1.custom_mode , packet1.system_status , packet1.voltage_battery , packet1.battery_remaining , packet1.drop_rate_comm , packet1.errors_drop , packet1.latitude , packet1.longitude , packet1.altitude , packet1.relative_alt , packet1.heading );
    mavlink_msg_fleet_uav_info_global_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_uav_request_local(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_uav_request_local_t packet_in = {
        17.0,45.0,73.0,41,108,175,242
    };
    mavlink_fleet_uav_request_local_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.x = packet_in.x;
        packet1.y = packet_in.y;
        packet1.z = packet_in.z;
        packet1.request_type = packet_in.request_type;
        packet1.request_ID = packet_in.request_ID;
        packet1.ID_to_replace = packet_in.ID_to_replace;
        packet1.request_urgency = packet_in.request_urgency;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_local_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_uav_request_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_local_pack(system_id, component_id, &msg , packet1.request_type , packet1.request_ID , packet1.ID_to_replace , packet1.x , packet1.y , packet1.z , packet1.request_urgency );
    mavlink_msg_fleet_uav_request_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_local_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.request_type , packet1.request_ID , packet1.ID_to_replace , packet1.x , packet1.y , packet1.z , packet1.request_urgency );
    mavlink_msg_fleet_uav_request_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_uav_request_local_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_local_send(MAVLINK_COMM_1 , packet1.request_type , packet1.request_ID , packet1.ID_to_replace , packet1.x , packet1.y , packet1.z , packet1.request_urgency );
    mavlink_msg_fleet_uav_request_local_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_uav_request_global(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_uav_request_global_t packet_in = {
        963497464,963497672,17651,163,230,41,108
    };
    mavlink_fleet_uav_request_global_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.latitude = packet_in.latitude;
        packet1.longitude = packet_in.longitude;
        packet1.altitude = packet_in.altitude;
        packet1.request_type = packet_in.request_type;
        packet1.request_ID = packet_in.request_ID;
        packet1.ID_to_replace = packet_in.ID_to_replace;
        packet1.request_urgency = packet_in.request_urgency;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_UAV_REQUEST_GLOBAL_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_global_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_uav_request_global_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_global_pack(system_id, component_id, &msg , packet1.request_type , packet1.request_ID , packet1.ID_to_replace , packet1.latitude , packet1.longitude , packet1.altitude , packet1.request_urgency );
    mavlink_msg_fleet_uav_request_global_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_global_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.request_type , packet1.request_ID , packet1.ID_to_replace , packet1.latitude , packet1.longitude , packet1.altitude , packet1.request_urgency );
    mavlink_msg_fleet_uav_request_global_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_uav_request_global_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_global_send(MAVLINK_COMM_1 , packet1.request_type , packet1.request_ID , packet1.ID_to_replace , packet1.latitude , packet1.longitude , packet1.altitude , packet1.request_urgency );
    mavlink_msg_fleet_uav_request_global_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_uav_request_response(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_UAV_REQUEST_RESPONSE >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_uav_request_response_t packet_in = {
        93372036854775807ULL,29
    };
    mavlink_fleet_uav_request_response_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.arrival_time = packet_in.arrival_time;
        packet1.request_ID = packet_in.request_ID;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_UAV_REQUEST_RESPONSE_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_UAV_REQUEST_RESPONSE_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_response_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_uav_request_response_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_response_pack(system_id, component_id, &msg , packet1.request_ID , packet1.arrival_time );
    mavlink_msg_fleet_uav_request_response_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_response_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.request_ID , packet1.arrival_time );
    mavlink_msg_fleet_uav_request_response_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_uav_request_response_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_request_response_send(MAVLINK_COMM_1 , packet1.request_ID , packet1.arrival_time );
    mavlink_msg_fleet_uav_request_response_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_uav_elected_id(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_uav_elected_id_t packet_in = {
        5,72
    };
    mavlink_fleet_uav_elected_id_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.request_ID = packet_in.request_ID;
        packet1.elected_ID = packet_in.elected_ID;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ID_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_id_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_uav_elected_id_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_id_pack(system_id, component_id, &msg , packet1.request_ID , packet1.elected_ID );
    mavlink_msg_fleet_uav_elected_id_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_id_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.request_ID , packet1.elected_ID );
    mavlink_msg_fleet_uav_elected_id_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_uav_elected_id_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_id_send(MAVLINK_COMM_1 , packet1.request_ID , packet1.elected_ID );
    mavlink_msg_fleet_uav_elected_id_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_uav_elected_accept(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_uav_elected_accept_t packet_in = {
        5
    };
    mavlink_fleet_uav_elected_accept_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.request_ID = packet_in.request_ID;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_accept_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_uav_elected_accept_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_accept_pack(system_id, component_id, &msg , packet1.request_ID );
    mavlink_msg_fleet_uav_elected_accept_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_accept_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.request_ID );
    mavlink_msg_fleet_uav_elected_accept_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_uav_elected_accept_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_uav_elected_accept_send(MAVLINK_COMM_1 , packet1.request_ID );
    mavlink_msg_fleet_uav_elected_accept_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_replacement_in_position(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_replacement_in_position_t packet_in = {
        5
    };
    mavlink_fleet_replacement_in_position_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.request_ID = packet_in.request_ID;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_REPLACEMENT_IN_POSITION_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_replacement_in_position_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_replacement_in_position_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_replacement_in_position_pack(system_id, component_id, &msg , packet1.request_ID );
    mavlink_msg_fleet_replacement_in_position_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_replacement_in_position_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.request_ID );
    mavlink_msg_fleet_replacement_in_position_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_replacement_in_position_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_replacement_in_position_send(MAVLINK_COMM_1 , packet1.request_ID );
    mavlink_msg_fleet_replacement_in_position_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_set_formation(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_SET_FORMATION >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_set_formation_t packet_in = {
        123.0,179.0,235.0,291.0,347.0,403.0,459.0,515.0,571.0,627.0,683.0,739.0,795.0,851.0,907.0,963.0,963504120,963504328,969.0,997.0,1025.0,24931,25035,205,16,83
    };
    mavlink_fleet_set_formation_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.d = packet_in.d;
        packet1.epsilon = packet_in.epsilon;
        packet1.FoV = packet_in.FoV;
        packet1.AtractMax = packet_in.AtractMax;
        packet1.RepulsionMax = packet_in.RepulsionMax;
        packet1.h_ = packet_in.h_;
        packet1.c1 = packet_in.c1;
        packet1.c2 = packet_in.c2;
        packet1.Kd_form = packet_in.Kd_form;
        packet1.Kp_form = packet_in.Kp_form;
        packet1.Ki_form = packet_in.Ki_form;
        packet1.T = packet_in.T;
        packet1.ux_sat = packet_in.ux_sat;
        packet1.uy_sat = packet_in.uy_sat;
        packet1.uz_sat = packet_in.uz_sat;
        packet1.Radius = packet_in.Radius;
        packet1.barycenter_lat = packet_in.barycenter_lat;
        packet1.barycenter_long = packet_in.barycenter_long;
        packet1.width_bound = packet_in.width_bound;
        packet1.length_bound = packet_in.length_bound;
        packet1.height_bound = packet_in.height_bound;
        packet1.barycenter_alt = packet_in.barycenter_alt;
        packet1.barycenter_relative_alt = packet_in.barycenter_relative_alt;
        packet1.formation_type = packet_in.formation_type;
        packet1.leader_ID = packet_in.leader_ID;
        packet1.norm_type = packet_in.norm_type;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_formation_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_set_formation_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_formation_pack(system_id, component_id, &msg , packet1.formation_type , packet1.leader_ID , packet1.barycenter_lat , packet1.barycenter_long , packet1.barycenter_alt , packet1.barycenter_relative_alt , packet1.width_bound , packet1.length_bound , packet1.height_bound , packet1.d , packet1.epsilon , packet1.FoV , packet1.AtractMax , packet1.RepulsionMax , packet1.h_ , packet1.c1 , packet1.c2 , packet1.Kd_form , packet1.Kp_form , packet1.Ki_form , packet1.T , packet1.ux_sat , packet1.uy_sat , packet1.uz_sat , packet1.Radius , packet1.norm_type );
    mavlink_msg_fleet_set_formation_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_formation_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.formation_type , packet1.leader_ID , packet1.barycenter_lat , packet1.barycenter_long , packet1.barycenter_alt , packet1.barycenter_relative_alt , packet1.width_bound , packet1.length_bound , packet1.height_bound , packet1.d , packet1.epsilon , packet1.FoV , packet1.AtractMax , packet1.RepulsionMax , packet1.h_ , packet1.c1 , packet1.c2 , packet1.Kd_form , packet1.Kp_form , packet1.Ki_form , packet1.T , packet1.ux_sat , packet1.uy_sat , packet1.uz_sat , packet1.Radius , packet1.norm_type );
    mavlink_msg_fleet_set_formation_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_set_formation_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_formation_send(MAVLINK_COMM_1 , packet1.formation_type , packet1.leader_ID , packet1.barycenter_lat , packet1.barycenter_long , packet1.barycenter_alt , packet1.barycenter_relative_alt , packet1.width_bound , packet1.length_bound , packet1.height_bound , packet1.d , packet1.epsilon , packet1.FoV , packet1.AtractMax , packet1.RepulsionMax , packet1.h_ , packet1.c1 , packet1.c2 , packet1.Kd_form , packet1.Kp_form , packet1.Ki_form , packet1.T , packet1.ux_sat , packet1.uy_sat , packet1.uz_sat , packet1.Radius , packet1.norm_type );
    mavlink_msg_fleet_set_formation_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_set_target_local(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_SET_TARGET_LOCAL >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_set_target_local_t packet_in = {
        17.0,45.0,73.0,101.0,18067,18171,65,132,199
    };
    mavlink_fleet_set_target_local_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.x = packet_in.x;
        packet1.y = packet_in.y;
        packet1.z = packet_in.z;
        packet1.perimeter = packet_in.perimeter;
        packet1.heading = packet_in.heading;
        packet1.drone_count = packet_in.drone_count;
        packet1.target_ID = packet_in.target_ID;
        packet1.target_type = packet_in.target_type;
        packet1.add_replace = packet_in.add_replace;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_SET_TARGET_LOCAL_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_SET_TARGET_LOCAL_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_local_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_set_target_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_local_pack(system_id, component_id, &msg , packet1.target_ID , packet1.target_type , packet1.x , packet1.y , packet1.z , packet1.heading , packet1.add_replace , packet1.drone_count , packet1.perimeter );
    mavlink_msg_fleet_set_target_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_local_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.target_ID , packet1.target_type , packet1.x , packet1.y , packet1.z , packet1.heading , packet1.add_replace , packet1.drone_count , packet1.perimeter );
    mavlink_msg_fleet_set_target_local_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_set_target_local_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_local_send(MAVLINK_COMM_1 , packet1.target_ID , packet1.target_type , packet1.x , packet1.y , packet1.z , packet1.heading , packet1.add_replace , packet1.drone_count , packet1.perimeter );
    mavlink_msg_fleet_set_target_local_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_set_target(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_SET_TARGET >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_set_target_t packet_in = {
        963497464,963497672,73.0,17859,17963,18067,18171,65,132,199
    };
    mavlink_fleet_set_target_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.latitude = packet_in.latitude;
        packet1.longitude = packet_in.longitude;
        packet1.perimeter = packet_in.perimeter;
        packet1.altitude = packet_in.altitude;
        packet1.relative_alt = packet_in.relative_alt;
        packet1.heading = packet_in.heading;
        packet1.drone_count = packet_in.drone_count;
        packet1.target_ID = packet_in.target_ID;
        packet1.target_type = packet_in.target_type;
        packet1.add_replace = packet_in.add_replace;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_set_target_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_pack(system_id, component_id, &msg , packet1.target_ID , packet1.target_type , packet1.latitude , packet1.longitude , packet1.altitude , packet1.relative_alt , packet1.heading , packet1.add_replace , packet1.drone_count , packet1.perimeter );
    mavlink_msg_fleet_set_target_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.target_ID , packet1.target_type , packet1.latitude , packet1.longitude , packet1.altitude , packet1.relative_alt , packet1.heading , packet1.add_replace , packet1.drone_count , packet1.perimeter );
    mavlink_msg_fleet_set_target_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_set_target_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_set_target_send(MAVLINK_COMM_1 , packet1.target_ID , packet1.target_type , packet1.latitude , packet1.longitude , packet1.altitude , packet1.relative_alt , packet1.heading , packet1.add_replace , packet1.drone_count , packet1.perimeter );
    mavlink_msg_fleet_set_target_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_clear_target(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_CLEAR_TARGET >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_clear_target_t packet_in = {
        5
    };
    mavlink_fleet_clear_target_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.target_ID = packet_in.target_ID;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_CLEAR_TARGET_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_CLEAR_TARGET_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_clear_target_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_clear_target_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_clear_target_pack(system_id, component_id, &msg , packet1.target_ID );
    mavlink_msg_fleet_clear_target_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_clear_target_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.target_ID );
    mavlink_msg_fleet_clear_target_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_clear_target_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_clear_target_send(MAVLINK_COMM_1 , packet1.target_ID );
    mavlink_msg_fleet_clear_target_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_fleet_target_stream_ready(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_fleet_target_stream_ready_t packet_in = {
        5
    };
    mavlink_fleet_target_stream_ready_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.target_ID = packet_in.target_ID;
        
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_target_stream_ready_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_fleet_target_stream_ready_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_target_stream_ready_pack(system_id, component_id, &msg , packet1.target_ID );
    mavlink_msg_fleet_target_stream_ready_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_target_stream_ready_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.target_ID );
    mavlink_msg_fleet_target_stream_ready_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_fleet_target_stream_ready_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_fleet_target_stream_ready_send(MAVLINK_COMM_1 , packet1.target_ID );
    mavlink_msg_fleet_target_stream_ready_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_HDS_AIRMES(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
    mavlink_test_fleet_uav_info_local(system_id, component_id, last_msg);
    mavlink_test_fleet_uav_info_global(system_id, component_id, last_msg);
    mavlink_test_fleet_uav_request_local(system_id, component_id, last_msg);
    mavlink_test_fleet_uav_request_global(system_id, component_id, last_msg);
    mavlink_test_fleet_uav_request_response(system_id, component_id, last_msg);
    mavlink_test_fleet_uav_elected_id(system_id, component_id, last_msg);
    mavlink_test_fleet_uav_elected_accept(system_id, component_id, last_msg);
    mavlink_test_fleet_replacement_in_position(system_id, component_id, last_msg);
    mavlink_test_fleet_set_formation(system_id, component_id, last_msg);
    mavlink_test_fleet_set_target_local(system_id, component_id, last_msg);
    mavlink_test_fleet_set_target(system_id, component_id, last_msg);
    mavlink_test_fleet_clear_target(system_id, component_id, last_msg);
    mavlink_test_fleet_target_stream_ready(system_id, component_id, last_msg);
}

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // HDS_AIRMES_TESTSUITE_H
