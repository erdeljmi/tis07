// %flair:license{
// This file is part of the Flair framework distributed under the
// CECILL-C License, Version 1.0.
// %flair:license}
//  created:    2013/11/17
//  filename:   Socket.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining a UDP socket
//
//
/*********************************************************************/
#include "Socket.h"
#include <string.h>
#include <stdexcept>

#include <fcntl.h>
#include <cstdlib>
#include <unistd.h>

using std::string;

Socket::Socket(uint16_t port) {
  pimpl_ = new Socket_impl(this, port);
}

Socket::Socket(string address, bool broadcast) {
  pimpl_ = new Socket_impl(this, address, broadcast);
}

Socket::~Socket() { delete pimpl_; }

void Socket::SendMessage(const char *message, size_t message_len) {
  pimpl_->SendMessage(message, message_len);
}

void Socket::SendMessage(string message) { pimpl_->SendMessage(message); }

ssize_t Socket::RecvMessage(char *buf, size_t buf_len, Time timeout, char *src, size_t *src_len) {
  return pimpl_->RecvMessage(buf, buf_len, timeout, src, src_len);
}

void Socket::NetworkToHost(char *data, size_t dataSize) {
  if(IsBigEndian())
    return;
  if (dataSize == 1)
    return;
  if ((dataSize == 2) || (dataSize == 4) || (dataSize == 8) ||
      (dataSize == 16)) {
    char dataInHostEndianness[dataSize];
    for (unsigned int i = 0; i < dataSize; i++) {
      dataInHostEndianness[i] = data[dataSize - i - 1];
    }
    memcpy(data, dataInHostEndianness, dataSize);
    return;
  }
  throw std::runtime_error(
      string("Unsupported data size (") + std::to_string(dataSize) +
      string(") in host to network endianness conversion"));
}

void Socket::HostToNetwork(char *data, size_t dataSize) {
  if (IsBigEndian())
    return;
  if (dataSize == 1)
    return;
  if ((dataSize == 2) || (dataSize == 4) || (dataSize == 8) ||
      (dataSize == 16)) {
    char dataInNetworkEndianness[dataSize];
    for (unsigned int i = 0; i < dataSize; i++) {
      dataInNetworkEndianness[i] = data[dataSize - i - 1];
    }
    memcpy(data, dataInNetworkEndianness, dataSize);
    return;
  }
  throw std::runtime_error(
      string("Unsupported data size (") + std::to_string(dataSize) +
      string(") in host to network endianness conversion"));
}

bool Socket::IsBigEndian(void) {
  union {
    uint32_t i;
    char c[4];
  } bint = {0x01020304};

  if (bint.c[0] == 1) {
    return true;
  } else {
    return false;
  }
}

Socket_impl::Socket_impl(const Socket *self, uint16_t port) {
    this->self = self;
    this->port = port;
    this->address = "";
    this->broadcast = false;
    Init();
}

Socket_impl::Socket_impl(const Socket *self, string address, bool broadcast) {
    this->self = self;
    int pos = address.find(":");
    this->address = address.substr(0, pos);
    port = atoi(address.substr(pos + 1).c_str());
    this->broadcast = broadcast;

    if (pos == 0 || address == "") {
        printf("address %s is not correct\n", address.c_str());
    }
    Init();
}

void Socket_impl::Init(void) {
    int yes = 1;

    fd = socket(AF_INET, SOCK_DGRAM, 0); // UDP

    if (broadcast == true) {
        if (setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(int)) != 0)
            printf("Setsockopt error\n");
    }

    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) != 0)
        printf("Setsockopt error\n");

    if (address == "" || broadcast == true) {
        sockaddr_in sin = {0};

        if (broadcast == true) {
            struct hostent *hostinfo;

            hostinfo = gethostbyname(this->address.c_str());
            if (hostinfo == NULL) {
                printf("hostinfo error\n");
            }
            sin.sin_addr = *(in_addr *)hostinfo->h_addr;
        } else {
            sin.sin_addr.s_addr = INADDR_ANY;
        }

        sin.sin_port = htons(port);
        sin.sin_family = AF_INET;
        if (bind(fd, (sockaddr *)&sin, sizeof sin) == -1) {
            printf("bind error\n");
        }
    }

    if (address != "") {
        struct hostent *hostinfo;
        hostinfo = gethostbyname(address.c_str());
        if (hostinfo == NULL) {
            printf("gethostbyname\n");
        }

        sock_in.sin_addr = *(in_addr *)hostinfo->h_addr;
        sock_in.sin_port = htons(port);
        sock_in.sin_family = AF_INET;
    }
}

Socket_impl::~Socket_impl() {
    close(fd);
}

void Socket_impl::SendMessage(const char *src, size_t src_len) {
    ssize_t written;
    string to_send;

    if (broadcast == true) {
        to_send = "GCS:" + string(src, src_len);
        src_len = to_send.size();
        src = (char *)to_send.c_str();
    }

    written = sendto(fd, src, src_len, 0, (struct sockaddr *)&sock_in, sizeof(sock_in));
    if(written==-1) {
        //printf("sendto error (%s)\n",strerror(-errno));
        printf("sendto error!!\n");
    } else if (written != (ssize_t)src_len) {
        printf("sendto error %i/%i\n",written,src_len);
    }
}

void Socket_impl::SendMessage(string message) {
    ssize_t written;

    if (broadcast == true) {
        message = "GCS:" + message;
    }
    // Printf("SendMessage %s\n",message.c_str());
    written = sendto(fd, message.c_str(), message.size(), 0, (struct sockaddr *)&sock_in, sizeof(sock_in));
    if (written != (ssize_t)message.size()) {
        printf("sendto error\n");
    }
}

ssize_t Socket_impl::RecvMessage(char *msg, size_t msg_len, Time timeout, char *src, size_t *src_len) {
    ssize_t nb_read;
    char buffer[128];
    socklen_t sinsize = sizeof(sock_in);
    struct timeval tv;

    if (timeout != TIME_NONBLOCK) {
        int attr = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, attr & (~O_NONBLOCK));

        tv.tv_sec = timeout / 1000000000;
        timeout = timeout - (timeout / 1000000000) * 1000000000;
        tv.tv_usec = timeout / 1000;
        if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
            printf("setsockopt SO_RCVTIMEO failed\n");
        }
    } else {
        fcntl(fd, F_SETFL, O_NONBLOCK);
    }

    if (broadcast == false) {
        nb_read = recvfrom(fd, buffer, sizeof(buffer), 0, (sockaddr *)&sock_in, &sinsize);
    } else {
        nb_read = recvfrom(fd, buffer, sizeof(buffer), 0, NULL, NULL);
    }
    if (nb_read <= 0) {
        return nb_read;
    } else {
        // printf("%s\n",buffer);
        if (broadcast == true) {
            int index = -1;
            for (int i = 0; i < nb_read; i++) {
                if (buffer[i] == ':') {
                    index = i;
                    break;
                }
            }
            if (index < 0) {
                printf("Malformed message\n");
                return -1;
            } else if (src_len != NULL && src != NULL) {
                if (index + 1 > (int)(*src_len) &&
                        src != NULL) { //+1 pour inserer un 0)
                    printf("insufficent src size\n");
                    return -1;
                }
            } else if (nb_read - index - 1 + 1 >
                       (int)msg_len) { //+1 pour inserer un 0
                printf("insufficent msg size (%i/%i)\n", nb_read - index - 1 + 1,
                           msg_len);
                return -1;
            }
            if (src != NULL) {
                memcpy(src, buffer, index);
                src[index] = 0;
            }
            memcpy(msg, &buffer[index + 1], nb_read - index - 1);
            msg[nb_read - index - 1] = 0;
            return nb_read - index;
        } else {
            if (nb_read > (int)msg_len) {
                printf("insufficent msg size (%i/%i)\n", nb_read, msg_len);
                return -1;
            }
            memcpy(msg, buffer, nb_read);
            return nb_read;
        }
    }
}

