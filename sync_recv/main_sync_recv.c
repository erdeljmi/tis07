// Milan Erdelj
// 28/12/2010

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <time.h>
#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <termios.h>

#define BUFFER_LEN 8
//uchar add_checksum(uchar *buff);
struct sockaddr_in myaddr_in;
struct sockaddr_in client_in;
int socket_in;
char buffer_in[BUFFER_LEN];
char local_time[25];
time_t now;
struct timeval tv;
struct timezone tz;
struct tm *tm;

int main(int argc, char *argv[])
{
    socklen_t clilen;
    time(&now);
    tm = localtime(&now);
    strftime(local_time,25,"%x_%X",tm);
    printf("Local time: %s\n",local_time);
    clilen=sizeof(client_in);
    if((socket_in=socket(AF_INET,SOCK_DGRAM,0))<0)
    {
        printf("udp in socket error\n");
        exit(1);
    }
    memset((char *)&myaddr_in, 0, sizeof(myaddr_in));
    myaddr_in.sin_family=AF_INET;
    myaddr_in.sin_addr.s_addr=htonl(INADDR_ANY);
    myaddr_in.sin_port=htons(15000);
    if(bind(socket_in,(struct sockaddr *)&myaddr_in,sizeof(myaddr_in))<0)
    {
        printf("udp in bind error\n");
        exit(1);
    }
    while(1)
{
    recvfrom(socket_in,buffer_in,BUFFER_LEN,0,(struct sockaddr *)&client_in,&clilen);
    //set the time
    //tv.tv_sec = ((long)buffer_in[0]<<24) + ((long)buffer_in[1]<<16) + ((long)buffer_in[2]<<8) + (long)buffer_in[3];
    //tv.tv_usec = ((long)buffer_in[4]<<24) + ((long)buffer_in[5]<<16) + ((long)buffer_in[6]<<8) + (long)buffer_in[7];
    //printf("%ld.%ld\n", tv.tv_sec, tv.tv_usec);
printf("%s",buffer_in);
    //printf("setting the time...\n");
}
    return 0;
}
