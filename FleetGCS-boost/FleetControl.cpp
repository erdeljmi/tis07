//  created:    03/04/2016
//  updated:    07/02/2017
//  filename:   FleetControl.cpp
//
//  author:     Milan Erdelj
//
//  version:    $Id: $
//
//  purpose:    Fleet control Qt application
//
//
/*********************************************************************/

#include "FleetControl.h"
#include "ui_fleetcontrol.h"

using namespace std;

FleetControl::FleetControl(QString address, int port, QString broadcast, int bcport, int sysid, unsigned short local_port, QWidget *parent) : QWidget(parent), ui(new Ui::FleetControl), mavlinkUDP(address.toStdString(),port,broadcast.toStdString(),bcport, sysid, local_port) {
    ui->setupUi(this);
    connect(ui->btnSetTarget, SIGNAL(clicked()), this, SLOT(SetTarget()));
    connect(ui->btnStartMission, SIGNAL(clicked()), this, SLOT(StartMission()));
    connect(ui->btnStopMission, SIGNAL(clicked()), this, SLOT(StopMission()));
    connect(ui->btnTakeOff, SIGNAL(clicked()), this, SLOT(fleetTakeOff()));
    connect(ui->btnLand, SIGNAL(clicked()), this, SLOT(fleetLand()));
    connect(ui->btnFormation, SIGNAL(clicked()), this, SLOT(SetFormation()));

    connect(net_manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(onfinish(QNetworkReply*)));

    // table view init
    ui->tblUAVs->setColumnCount(COLUMN_COUNT);
    ui->tblUAVs->setRowCount(ROW_COUNT);
    ui->tblUAVs->verticalHeader()->setStyleSheet("color: black");
    ui->tblUAVs->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tblUAVs->verticalHeader()->setVisible(false);
    ui->tblUAVs->horizontalHeader()->setStretchLastSection(true);
    QStringList header_labels;
    header_labels<<"System ID"<<"X"<<"Y"<<"Z"<<"Lat"<<"Long"<<"Custom state";
    for(unsigned int i = 0; i<ROW_COUNT; i++) {
        for(unsigned int j = 0; j<COLUMN_COUNT; j++) {
            ui->tblUAVs->setItem(i, j, new QTableWidgetItem(""));
            ui->tblUAVs->item(i,j)->setTextAlignment(Qt::AlignCenter);
        }
    }
    ui->tblUAVs->setHorizontalHeaderLabels(header_labels);
    clearTable();

    // geo conv
    geoConv.initialiseReference(49.402214, 2.795260, 5);

    // setpoint values initialization
    targetX = targetY = 3.0;
    targetZ = 1.0;
    targetR = 2.0;
    target_id = 0;
    ui->spinX->setValue(targetX);
    ui->spinY->setValue(targetY);
    ui->spinZ->setValue(targetZ);
    ui->spinR->setValue(targetR);

    target_system_id = 255;
    target_comp_id = 255;

    heartbeat_period = HEARTBEAT_DEFAULT_PERIOD;

    // heartbeat flags
    heartbeat_uav_type = MAV_TYPE_GCS;
    uav_autopilot = MAV_AUTOPILOT_GENERIC;
    uav_base_mode = MAV_MODE_FLAG_SAFETY_ARMED;
    uav_custom_mode = 0;
    system_status = MAV_STATE_UNINIT;

    // period function flags
    sendingPosition = false;
    sendingHeartbeat = true;
    sendingPositionInterval = POSITION_DEFAULT_PERIOD;

    // set the uav mode to standby
    system_status = MAV_STATE_STANDBY;

    timerSendTarget = new QTimer(this);
    connect(timerSendTarget, SIGNAL(timeout()), this, SLOT(callbackSendTarget()));
    timerUpdateNeighTable = new QTimer(this);
    connect(timerUpdateNeighTable, SIGNAL(timeout()), this, SLOT(RefreshNeighTable()));
    timerInfoUAV = new QTimer(this);
    connect(timerInfoUAV, SIGNAL(timeout()), this, SLOT(callbackInfoUAV()));

    timerInfoUAV->start(1000); // milliseconds
    timerUpdateNeighTable->start(500);
}

FleetControl::~FleetControl() {
    delete net_manager;
    delete request;
    delete ui;
}

void FleetControl::StartMission() {
    mavlinkUDP.cmdFleetStartMission(target_system_id, target_comp_id, 0);
}

void FleetControl::StopMission() {
    mavlinkUDP.cmdFleetEndMission(target_system_id, target_comp_id, 0);
    timerSendTarget->stop();
}


void FleetControl::fleetTakeOff() {
    //mavlinkUDP.cmdNavTakeoff(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    mavlinkUDP.cmdNavTakeoffBroadcast(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    // ACK will be implemented when sending unicast to everyone
    //mavlinkUDP.waitCommandAck(ACK_TIMEOUT);
}

void FleetControl::fleetLand() {
    //mavlinkUDP.cmdNavLand(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    mavlinkUDP.cmdNavLandBroadcast(target_system_id, target_comp_id, 0, 0, 0, 0, 0);
    // ACK will be implemented when sending unicast to everyone
    //mavlinkUDP.waitCommandAck(ACK_TIMEOUT);
    timerSendTarget->stop();
}

void FleetControl::SetTarget() {
    // change the values
    targetX = ui->spinX->value();
    targetY = ui->spinY->value();
    targetZ = ui->spinZ->value();
    targetR = ui->spinR->value();
    switch(ui->comboTargetType->currentIndex()) {
    case 0:
        // undefined
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_UNDEFINED, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    case 1:
        // point
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_POINT, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    case 2:
        // structure
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_STRUCTURE, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    case 3:
        // crowd
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_CROWD, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    case 4:
        // emergency
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_EMERGENCY, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    case 5:
        // delivery
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_DELIVERY, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    case 6:
        // barrier
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_BARRIER, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    case 7:
        // other
        mavlinkUDP.sendFleetSetTargetLocal(target_id, FLEET_TARGET_OTHER, targetX, targetY, targetZ, 0, 0, 0, targetR);
        break;
    default:
        break;
    }
}

void FleetControl::SetFormation() {
    // stop Setpoint sending
    timerSendTarget->stop();
    switch(ui->comboFormation->currentIndex()) {
    case 0:
        // undefined
        mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_UNDEFINED, 0, 0, 0, 0, 0, 0, 0, 0, formation.d, formation.epsilon, formation.FoV, formation.AttractMax, formation.RepulsionMax, formation.h_, formation.c1, formation.c2, formation.Kd_form, formation.Kp_form, formation.Ki_form, formation.T, formation.ux_sat, formation.uy_sat, formation.uz_sat, formation.Radius, formation.norm_type);
        break;
    case 1:
        // ring
        //mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_RING, 0, 0, 0, 0, 0, 0, 0, 0);
        mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_RING, 0, 0, 0, 0, 0, 0, 0, 0, formation.d, formation.epsilon, formation.FoV, formation.AttractMax, formation.RepulsionMax, formation.h_, formation.c1, formation.c2, formation.Kd_form, formation.Kp_form, formation.Ki_form, formation.T, formation.ux_sat, formation.uy_sat, formation.uz_sat, formation.Radius, formation.norm_type);
        break;
    case 2:
        // column
        //mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_COLUMN, 0, 0, 0, 0, 0, 0, 0, 0);
        mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_COLUMN, 0, 0, 0, 0, 0, 0, 0, 0, formation.d, formation.epsilon, formation.FoV, formation.AttractMax, formation.RepulsionMax, formation.h_, formation.c1, formation.c2, formation.Kd_form, formation.Kp_form, formation.Ki_form, formation.T, formation.ux_sat, formation.uy_sat, formation.uz_sat, formation.Radius, formation.norm_type);
        break;
    case 3:
        // line
        //mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_LINE, 0, 0, 0, 0, 0, 0, 0, 0);
        mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_LINE, 0, 0, 0, 0, 0, 0, 0, 0, formation.d, formation.epsilon, formation.FoV, formation.AttractMax, formation.RepulsionMax, formation.h_, formation.c1, formation.c2, formation.Kd_form, formation.Kp_form, formation.Ki_form, formation.T, formation.ux_sat, formation.uy_sat, formation.uz_sat, formation.Radius, formation.norm_type);
        break;
    case 4:
        // square
        //mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_SQUARE, 0, 0, 0, 0, 0, 0, 0, 0);
        mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_SQUARE, 0, 0, 0, 0, 0, 0, 0, 0, formation.d, formation.epsilon, formation.FoV, formation.AttractMax, formation.RepulsionMax, formation.h_, formation.c1, formation.c2, formation.Kd_form, formation.Kp_form, formation.Ki_form, formation.T, formation.ux_sat, formation.uy_sat, formation.uz_sat, formation.Radius, formation.norm_type);
        break;
    case 5:
        // wedge
        //mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_WEDGE, 0, 0, 0, 0, 0, 0, 0, 0);
        mavlinkUDP.sendFleetSetFormation(FLEET_FORMATION_WEDGE, 0, 0, 0, 0, 0, 0, 0, 0, formation.d, formation.epsilon, formation.FoV, formation.AttractMax, formation.RepulsionMax, formation.h_, formation.c1, formation.c2, formation.Kd_form, formation.Kp_form, formation.Ki_form, formation.T, formation.ux_sat, formation.uy_sat, formation.uz_sat, formation.Radius, formation.norm_type);
        break;
    default:
        break;
    }
}

void FleetControl::clearTable() {
    for(unsigned int i = 0; i<ROW_COUNT; i++) {
        for(unsigned int j = 0; j<COLUMN_COUNT; j++) {
            ui->tblUAVs->item(i,j)->setText("");
        }
    }
}

void FleetControl::RefreshNeighTable() {
    uint8_t num_elements = 0;
    uint64_t latency;
    std::set<uint8_t>::iterator it;
    // remove old entries
    it = mavlinkUDP.activeUAVs.begin();
    for(; it != mavlinkUDP.activeUAVs.end(); ) {
        latency = mavlinkUDP.get_time_usec() - mavlinkUDP.neighUAVs[*it].last_update;
        if(latency > UAV_INFO_TIMEOUT) {
            // exclude the drone
            mavlinkUDP.activeUAVs.erase(it++);
            //
            // TODO remove the drone!
            //
        } else {
            // keep the drone info
            num_elements++;
            ++it;
        }
    }
    clearTable();
    // populate the tablewidget
    uint8_t count = 0;
    for(it = mavlinkUDP.activeUAVs.begin(); it != mavlinkUDP.activeUAVs.end(); ++it) {
        uint8_t current_id = *it;
        //printf("ACTIVE_UAVS: id=%d=%d x=%.2f y=%.2f z=%.2f custom_state=%d\n", current_id, mavlinkUDP.infoUAV[current_id].system_id, mavlinkUDP.infoUAV[current_id].x, mavlinkUDP.infoUAV[current_id].y, mavlinkUDP.infoUAV[current_id].z, mavlinkUDP.infoUAV[current_id].custom_mode);
        ui->tblUAVs->item(count,0)->setText(QString::number(current_id));
        ui->tblUAVs->item(count,1)->setText(QString::number(mavlinkUDP.neighUAVs[current_id].x));
        ui->tblUAVs->item(count,2)->setText(QString::number(mavlinkUDP.neighUAVs[current_id].y));
        ui->tblUAVs->item(count,3)->setText(QString::number(mavlinkUDP.neighUAVs[current_id].z));

        // conversion to geodetic
        double latitude, longitude, altitude;
        printf("ID %d LAT %lf LONG %lf\n", current_id, latitude, longitude);
        geoConv.ned2Geodetic(mavlinkUDP.neighUAVs[current_id].x, mavlinkUDP.neighUAVs[current_id].y,mavlinkUDP.neighUAVs[current_id].z, &latitude, &longitude, &altitude);

        ui->tblUAVs->item(count,4)->setText(QString::number(latitude));
        ui->tblUAVs->item(count,5)->setText(QString::number(longitude));

        mavlinkUDP.neighUAVs[current_id].latitude = (uint32_t)(latitude*10000000);
        mavlinkUDP.neighUAVs[current_id].longitude = (uint32_t)(longitude*10000000);
        mavlinkUDP.neighUAVs[current_id].altitude = (uint32_t)altitude;

        // prepare JSON
        QJsonObject json;
        json.insert("system_id",mavlinkUDP.neighUAVs[current_id].system_id);
        QString address = "192.168.6." + QString::number(mavlinkUDP.neighUAVs[current_id].system_id);
        json.insert("ip_address",address);
        json.insert("latitude",mavlinkUDP.neighUAVs[current_id].latitude);
        json.insert("longitude",mavlinkUDP.neighUAVs[current_id].longitude);
        json.insert("altitude",mavlinkUDP.neighUAVs[current_id].altitude);
        json.insert("relative_alt",mavlinkUDP.neighUAVs[current_id].relative_alt);
        json.insert("heading",mavlinkUDP.neighUAVs[current_id].heading);
        json.insert("type",mavlinkUDP.neighUAVs[current_id].type);
        json.insert("autopilot",mavlinkUDP.neighUAVs[current_id].autopilot);
        json.insert("base_mode",mavlinkUDP.neighUAVs[current_id].base_mode);
        json.insert("custom_mode",mavlinkUDP.neighUAVs[current_id].custom_mode);
        json.insert("system_status",mavlinkUDP.neighUAVs[current_id].system_status);
        json.insert("voltage_battery",mavlinkUDP.neighUAVs[current_id].voltage_battery);
        json.insert("drop_rate_comm",mavlinkUDP.neighUAVs[current_id].drop_rate_comm);
        json.insert("errors_comm",mavlinkUDP.neighUAVs[current_id].errors_comm);
        json.insert("vx",mavlinkUDP.neighUAVs[current_id].vx);
        json.insert("vy",mavlinkUDP.neighUAVs[current_id].vy);
        json.insert("vz",mavlinkUDP.neighUAVs[current_id].vz);
        json.insert("sensors_present",(uint8_t)mavlinkUDP.neighUAVs[current_id].sensors_present);
        json.insert("sensors_enabled",(uint8_t)mavlinkUDP.neighUAVs[current_id].sensors_enabled);
        json.insert("sensors_health",(uint8_t)mavlinkUDP.neighUAVs[current_id].sensors_health);
        json.insert("mainloop_load",mavlinkUDP.neighUAVs[current_id].mainloop_load);
        json.insert("current_battery",mavlinkUDP.neighUAVs[current_id].current_battery);
        json.insert("battery_remaining",mavlinkUDP.neighUAVs[current_id].battery_remaining);

        QJsonDocument json_doc(json);
        QByteArray json_bytes = json_doc.toJson();
        QByteArray postDataSize = QByteArray::number(json_bytes.size());

        // Add the headers specifying their names and their values
        request->setRawHeader("User-Agent", "StationFleet");
        request->setRawHeader("X-Custom-User-Agent", "StationFleet");
        request->setRawHeader("Content-Type", "application/x-www-form-urlencoded");
        request->setRawHeader("Content-Length", postDataSize);
        request->setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        // upload request
        net_manager->post(*request,json_bytes);

        switch(mavlinkUDP.neighUAVs[current_id].custom_mode) {
        case 0:
            // HANDOVER_STATE_INIT
            ui->tblUAVs->item(count,6)->setText("INIT");
            break;
        case 1:
            // HANDOVER_STATE_STANDBY
            ui->tblUAVs->item(count,6)->setText("STANDBY");
            break;
        case 2:
            // HANDOVER_STATE_MISSION_OFFER
            ui->tblUAVs->item(count,6)->setText("MISSION_OFFER");
            break;
        case 3:
            // HANDOVER_STATE_MISSION_EXECUTION
            ui->tblUAVs->item(count,6)->setText("MISSION_EXEC");
            break;
        case 4:
            // HANDOVER_STATE_REPLACEMENT_OFFER
            ui->tblUAVs->item(count,6)->setText("REPLACEMENT_OFFER");
            break;
        case 5:
            // HANDOVER_STATE_POSITIONING
            ui->tblUAVs->item(count,6)->setText("POSITIONING");
            break;
        case 6:
            // HANDOVER_STATE_REPLACEMENT_REQUEST
            ui->tblUAVs->item(count,6)->setText("REPLACEMENT_REQUEST");
            break;
        case 7:
            // HANDOVER_STATE_RECHARGING
            ui->tblUAVs->item(count,6)->setText("RECHARGING");
            break;
        default:
            break;
        }
        count++;
    }
    // tablewidget has to be refreshed
    ui->tblUAVs->viewport()->update();
}

void FleetControl::callbackHeartbeat() {
    mavlinkUDP.sendHeartbeat(heartbeat_uav_type, uav_autopilot, uav_base_mode, uav_custom_mode, system_status);
}

void FleetControl::callbackInfoUAV() {
    // sending HEARTBEAT, SYS_STATUS and LOCAL_POSITION_NED
    mavlinkUDP.sendInfoUAV(heartbeat_uav_type, uav_autopilot, uav_base_mode, uav_custom_mode, system_status, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    // sending custom info message
    //mavlinkUDP.sendFleetUavInfoLocal(uav_base_mode, uav_custom_mode, system_status, 0, 0, 0, 0, 0, 0, 0, 0);
}

void FleetControl::callbackUpdateNeighTable() {
    RefreshNeighTable();
}

void FleetControl::callbackSendTarget() {
    // send set_target_local_ned with current setpoint coordinates
    targetX = ui->spinX->value();
    targetY = ui->spinY->value();
    targetZ = ui->spinZ->value();
    targetR = ui->spinR->value();
    switch(ui->comboTargetType->currentIndex()) {
    case 0:
        // undefined
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_UNDEFINED, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    case 1:
        // point
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_POINT, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    case 2:
        // structure
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_STRUCTURE, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    case 3:
        // crowd
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_CROWD, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    case 4:
        // emergency
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_EMERGENCY, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    case 5:
        // delivery
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_DELIVERY, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    case 6:
        // barrier
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_BARRIER, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    case 7:
        // other
        mavlinkUDP.sendFleetSetTarget(target_id, FLEET_TARGET_OTHER, targetX, targetY, targetZ, 0, 0, 0, 0, targetR);
        break;
    default:
        break;
    }
}

void FleetControl::onfinish(QNetworkReply *reply)
{
    /*
     * Reply is finished!
     * We'll ask for the reply about the Redirection attribute
     * http://doc.trolltech.com/qnetworkrequest.html#Attribute-enum
     */

    ui->textBox->clear();

    QVariant possibleRedirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

    /* We'll deduct if the redirection is valid in the redirectUrl function */
    _urlRedirectedTo = this->redirectUrl(possibleRedirectUrl.toUrl(),_urlRedirectedTo);

    /* If the URL is not empty, we're being redirected. */
    if(!_urlRedirectedTo.isEmpty()) {
        QString text = QString("QNAMRedirect::replyFinished: Redirected to ").append(_urlRedirectedTo.toString());
        ui->textBox->append(text);

        /* We'll do another request to the redirection url. */
        net_manager->get(QNetworkRequest(_urlRedirectedTo));
    }
    else {
        /*
         * We weren't redirected anymore
         * so we arrived to the final destination...
         */
        QString text = QString("QNAMRedirect::replyFinished: Arrived to ").append(reply->url().toString());
        ui->textBox->append(text);

        /* ...so this can be cleared. */
        _urlRedirectedTo.clear();
    }

    /// try to get the data
    QByteArray bts = reply->readAll();
    QString str(bts);
    ui->textBox->append(str);
    //QMessageBox::information(this,"sal",str,"ok");

    /* Clean up. */
    reply->deleteLater();
}

QUrl FleetControl::redirectUrl(const QUrl& possibleRedirectUrl, const QUrl& oldRedirectUrl) const {
    QUrl redirectUrl;
    /*
     * Check if the URL is empty and
     * that we aren't being fooled into a infinite redirect loop.
     * We could also keep track of how many redirects we have been to
     * and set a limit to it, but we'll leave that to you.
     */
    if(!possibleRedirectUrl.isEmpty() &&
            possibleRedirectUrl != oldRedirectUrl) {
        redirectUrl = possibleRedirectUrl;
    }
    return redirectUrl;
}
