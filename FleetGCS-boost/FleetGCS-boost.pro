#-------------------------------------------------
#
# Project created by QtCreator 2016-07-21T23:20:55
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS = -std=c++11
QMAKE_LFLAGS = -std=c++11

TARGET = FleetGCS-boost
TEMPLATE = app

LIBS += -lboost_system\
        -lboost_thread

SOURCES += main.cpp\
    FleetControl.cpp \
    Socket.cpp \
    MavlinkServer.cpp

HEADERS  += \
    include/common/mavlink.h \
    FleetControl.h \
    NeighUAV.h \
    Socket.h \
    geodetic_conv.hpp \
    locked_queue.h \
    MavlinkServer.h

FORMS    += \
    fleetcontrol.ui
