/********************************************************************************
** Form generated from reading UI file 'fleetcontrol.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FLEETCONTROL_H
#define UI_FLEETCONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FleetControl
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QTableWidget *tblUAVs;
    QGroupBox *groupBox_7;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    QComboBox *comboFormation;
    QPushButton *btnFormation;
    QSpacerItem *verticalSpacer_3;
    QComboBox *comboTargetType;
    QFormLayout *formLayout;
    QLabel *label_3;
    QDoubleSpinBox *spinX;
    QLabel *label_4;
    QDoubleSpinBox *spinY;
    QDoubleSpinBox *spinZ;
    QDoubleSpinBox *spinR;
    QLabel *label_5;
    QLabel *label_16;
    QPushButton *btnSetTarget;
    QFrame *line;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *btnTakeOff;
    QPushButton *btnLand;
    QPushButton *btnStartMission;
    QPushButton *btnStopMission;
    QSpacerItem *verticalSpacer;
    QTextEdit *textBox;

    void setupUi(QWidget *FleetControl)
    {
        if (FleetControl->objectName().isEmpty())
            FleetControl->setObjectName(QStringLiteral("FleetControl"));
        FleetControl->resize(941, 479);
        verticalLayout = new QVBoxLayout(FleetControl);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        groupBox_2 = new QGroupBox(FleetControl);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        tblUAVs = new QTableWidget(groupBox_2);
        tblUAVs->setObjectName(QStringLiteral("tblUAVs"));

        verticalLayout_2->addWidget(tblUAVs);


        horizontalLayout->addWidget(groupBox_2);

        groupBox_7 = new QGroupBox(FleetControl);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        horizontalLayout_3 = new QHBoxLayout(groupBox_7);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        comboFormation = new QComboBox(groupBox_7);
        comboFormation->setObjectName(QStringLiteral("comboFormation"));

        verticalLayout_6->addWidget(comboFormation);

        btnFormation = new QPushButton(groupBox_7);
        btnFormation->setObjectName(QStringLiteral("btnFormation"));

        verticalLayout_6->addWidget(btnFormation);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_3);

        comboTargetType = new QComboBox(groupBox_7);
        comboTargetType->setObjectName(QStringLiteral("comboTargetType"));

        verticalLayout_6->addWidget(comboTargetType);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_3 = new QLabel(groupBox_7);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        spinX = new QDoubleSpinBox(groupBox_7);
        spinX->setObjectName(QStringLiteral("spinX"));
        spinX->setDecimals(1);
        spinX->setMinimum(-15);
        spinX->setMaximum(15);
        spinX->setValue(10);

        formLayout->setWidget(1, QFormLayout::FieldRole, spinX);

        label_4 = new QLabel(groupBox_7);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_4);

        spinY = new QDoubleSpinBox(groupBox_7);
        spinY->setObjectName(QStringLiteral("spinY"));
        spinY->setDecimals(1);
        spinY->setMinimum(-15);
        spinY->setMaximum(15);
        spinY->setValue(10);

        formLayout->setWidget(2, QFormLayout::FieldRole, spinY);

        spinZ = new QDoubleSpinBox(groupBox_7);
        spinZ->setObjectName(QStringLiteral("spinZ"));
        spinZ->setDecimals(1);
        spinZ->setMinimum(-15);
        spinZ->setMaximum(15);
        spinZ->setValue(1);

        formLayout->setWidget(3, QFormLayout::FieldRole, spinZ);

        spinR = new QDoubleSpinBox(groupBox_7);
        spinR->setObjectName(QStringLiteral("spinR"));
        spinR->setDecimals(1);
        spinR->setMinimum(0);
        spinR->setMaximum(15);
        spinR->setSingleStep(1);
        spinR->setValue(2);

        formLayout->setWidget(4, QFormLayout::FieldRole, spinR);

        label_5 = new QLabel(groupBox_7);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_5);

        label_16 = new QLabel(groupBox_7);
        label_16->setObjectName(QStringLiteral("label_16"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_16);


        verticalLayout_6->addLayout(formLayout);

        btnSetTarget = new QPushButton(groupBox_7);
        btnSetTarget->setObjectName(QStringLiteral("btnSetTarget"));

        verticalLayout_6->addWidget(btnSetTarget);


        horizontalLayout_3->addLayout(verticalLayout_6);

        line = new QFrame(groupBox_7);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout_3->addWidget(line);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        btnTakeOff = new QPushButton(groupBox_7);
        btnTakeOff->setObjectName(QStringLiteral("btnTakeOff"));

        horizontalLayout_5->addWidget(btnTakeOff);

        btnLand = new QPushButton(groupBox_7);
        btnLand->setObjectName(QStringLiteral("btnLand"));

        horizontalLayout_5->addWidget(btnLand);


        verticalLayout_8->addLayout(horizontalLayout_5);

        btnStartMission = new QPushButton(groupBox_7);
        btnStartMission->setObjectName(QStringLiteral("btnStartMission"));

        verticalLayout_8->addWidget(btnStartMission);

        btnStopMission = new QPushButton(groupBox_7);
        btnStopMission->setObjectName(QStringLiteral("btnStopMission"));

        verticalLayout_8->addWidget(btnStopMission);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_8->addItem(verticalSpacer);


        horizontalLayout_3->addLayout(verticalLayout_8);


        horizontalLayout->addWidget(groupBox_7);


        verticalLayout->addLayout(horizontalLayout);

        textBox = new QTextEdit(FleetControl);
        textBox->setObjectName(QStringLiteral("textBox"));

        verticalLayout->addWidget(textBox);


        retranslateUi(FleetControl);

        comboFormation->setCurrentIndex(1);
        comboTargetType->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(FleetControl);
    } // setupUi

    void retranslateUi(QWidget *FleetControl)
    {
        FleetControl->setWindowTitle(QApplication::translate("FleetControl", "MissionPlanner", 0));
        groupBox_2->setTitle(QApplication::translate("FleetControl", "Fleet status", 0));
        groupBox_7->setTitle(QApplication::translate("FleetControl", "Target management", 0));
        comboFormation->clear();
        comboFormation->insertItems(0, QStringList()
         << QApplication::translate("FleetControl", "FORMATION_UNDEFINED", 0)
         << QApplication::translate("FleetControl", "FORMATION_RING", 0)
         << QApplication::translate("FleetControl", "FORMATION_COLUMN", 0)
         << QApplication::translate("FleetControl", "FORMATION_LINE", 0)
         << QApplication::translate("FleetControl", "FORMATION_SQUARE", 0)
         << QApplication::translate("FleetControl", "FORMATION_WEDGE", 0)
         << QApplication::translate("FleetControl", "FORMATION_ECHELON", 0)
        );
        comboFormation->setCurrentText(QApplication::translate("FleetControl", "FORMATION_RING", 0));
        btnFormation->setText(QApplication::translate("FleetControl", "SET FORMATION", 0));
        comboTargetType->clear();
        comboTargetType->insertItems(0, QStringList()
         << QApplication::translate("FleetControl", "FLEET_TARGET_UNDEFINED", 0)
         << QApplication::translate("FleetControl", "FLEET_TARGET_POINT", 0)
         << QApplication::translate("FleetControl", "FLEET_TARGET_STRUCTURE", 0)
         << QApplication::translate("FleetControl", "FLEET_TARGET_CROWD", 0)
         << QApplication::translate("FleetControl", "FLEET_TARGET_EMERGENCY", 0)
         << QApplication::translate("FleetControl", "FLEET_TARGET_DELIVERY", 0)
         << QApplication::translate("FleetControl", "FLEET_TARGET_BARRIER", 0)
         << QApplication::translate("FleetControl", "FLEET_TARGET_OTHER", 0)
        );
        comboTargetType->setCurrentText(QApplication::translate("FleetControl", "FLEET_TARGET_POINT", 0));
        label_3->setText(QApplication::translate("FleetControl", "X:", 0));
        label_4->setText(QApplication::translate("FleetControl", "Y:", 0));
        label_5->setText(QApplication::translate("FleetControl", "Z:", 0));
        label_16->setText(QApplication::translate("FleetControl", "R:", 0));
        btnSetTarget->setText(QApplication::translate("FleetControl", "SET TARGET", 0));
        btnTakeOff->setText(QApplication::translate("FleetControl", "TAKEOFF", 0));
        btnLand->setText(QApplication::translate("FleetControl", "LAND", 0));
        btnStartMission->setText(QApplication::translate("FleetControl", "START", 0));
        btnStopMission->setText(QApplication::translate("FleetControl", "STOP", 0));
    } // retranslateUi

};

namespace Ui {
    class FleetControl: public Ui_FleetControl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FLEETCONTROL_H
