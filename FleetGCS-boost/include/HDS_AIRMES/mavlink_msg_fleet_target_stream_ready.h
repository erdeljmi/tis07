#pragma once
// MESSAGE FLEET_TARGET_STREAM_READY PACKING

#define MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY 180

MAVPACKED(
typedef struct __mavlink_fleet_target_stream_ready_t {
 uint8_t target_ID; /*<  Target ID*/
}) mavlink_fleet_target_stream_ready_t;

#define MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN 1
#define MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN 1
#define MAVLINK_MSG_ID_180_LEN 1
#define MAVLINK_MSG_ID_180_MIN_LEN 1

#define MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC 152
#define MAVLINK_MSG_ID_180_CRC 152



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_TARGET_STREAM_READY { \
    180, \
    "FLEET_TARGET_STREAM_READY", \
    1, \
    {  { "target_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_target_stream_ready_t, target_ID) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_TARGET_STREAM_READY { \
    "FLEET_TARGET_STREAM_READY", \
    1, \
    {  { "target_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_target_stream_ready_t, target_ID) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_target_stream_ready message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target_ID  Target ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_target_stream_ready_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN];
    _mav_put_uint8_t(buf, 0, target_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN);
#else
    mavlink_fleet_target_stream_ready_t packet;
    packet.target_ID = target_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC);
}

/**
 * @brief Pack a fleet_target_stream_ready message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_ID  Target ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_target_stream_ready_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN];
    _mav_put_uint8_t(buf, 0, target_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN);
#else
    mavlink_fleet_target_stream_ready_t packet;
    packet.target_ID = target_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC);
}

/**
 * @brief Encode a fleet_target_stream_ready struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_target_stream_ready C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_target_stream_ready_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_target_stream_ready_t* fleet_target_stream_ready)
{
    return mavlink_msg_fleet_target_stream_ready_pack(system_id, component_id, msg, fleet_target_stream_ready->target_ID);
}

/**
 * @brief Encode a fleet_target_stream_ready struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_target_stream_ready C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_target_stream_ready_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_target_stream_ready_t* fleet_target_stream_ready)
{
    return mavlink_msg_fleet_target_stream_ready_pack_chan(system_id, component_id, chan, msg, fleet_target_stream_ready->target_ID);
}

/**
 * @brief Send a fleet_target_stream_ready message
 * @param chan MAVLink channel to send the message
 *
 * @param target_ID  Target ID
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_target_stream_ready_send(mavlink_channel_t chan, uint8_t target_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN];
    _mav_put_uint8_t(buf, 0, target_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY, buf, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC);
#else
    mavlink_fleet_target_stream_ready_t packet;
    packet.target_ID = target_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY, (const char *)&packet, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC);
#endif
}

/**
 * @brief Send a fleet_target_stream_ready message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_target_stream_ready_send_struct(mavlink_channel_t chan, const mavlink_fleet_target_stream_ready_t* fleet_target_stream_ready)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_target_stream_ready_send(chan, fleet_target_stream_ready->target_ID);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY, (const char *)fleet_target_stream_ready, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_target_stream_ready_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, target_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY, buf, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC);
#else
    mavlink_fleet_target_stream_ready_t *packet = (mavlink_fleet_target_stream_ready_t *)msgbuf;
    packet->target_ID = target_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY, (const char *)packet, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_MIN_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_TARGET_STREAM_READY UNPACKING


/**
 * @brief Get field target_ID from fleet_target_stream_ready message
 *
 * @return  Target ID
 */
static inline uint8_t mavlink_msg_fleet_target_stream_ready_get_target_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Decode a fleet_target_stream_ready message into a struct
 *
 * @param msg The message to decode
 * @param fleet_target_stream_ready C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_target_stream_ready_decode(const mavlink_message_t* msg, mavlink_fleet_target_stream_ready_t* fleet_target_stream_ready)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_target_stream_ready->target_ID = mavlink_msg_fleet_target_stream_ready_get_target_ID(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN? msg->len : MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN;
        memset(fleet_target_stream_ready, 0, MAVLINK_MSG_ID_FLEET_TARGET_STREAM_READY_LEN);
    memcpy(fleet_target_stream_ready, _MAV_PAYLOAD(msg), len);
#endif
}
