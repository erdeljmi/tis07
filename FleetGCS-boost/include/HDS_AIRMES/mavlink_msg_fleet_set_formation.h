#pragma once
// MESSAGE FLEET_SET_FORMATION PACKING

#define MAVLINK_MSG_ID_FLEET_SET_FORMATION 170

MAVPACKED(
typedef struct __mavlink_fleet_set_formation_t {
 double d; /*<  Security interdistace between UAVs*/
 double epsilon; /*<  sigma norm parameter*/
 double FoV; /*<  Field of view of a UAV*/
 double AtractMax; /*<  Maximal attraction force*/
 double RepulsionMax; /*<  Maximal repulsion force*/
 double h_; /*<  Potention function cuttoff parameter*/
 double c1; /*<  Navigation proportional gain*/
 double c2; /*<  Navigation derivative gain*/
 double Kd_form; /*<  Formation derivative gain*/
 double Kp_form; /*<  Formation proportional gain*/
 double Ki_form; /*<  Formation integral gain*/
 double T; /*<  user defined delta T*/
 double ux_sat; /*<  Formation x control output saturation*/
 double uy_sat; /*<  Formation y control output saturation*/
 double uz_sat; /*<  Formation z control output saturation*/
 double Radius; /*<  Circle radius*/
 uint32_t barycenter_lat; /*<  Fleet barycenter latitude*/
 uint32_t barycenter_long; /*<  Fleet barycenter longitude*/
 float width_bound; /*<  Maximal fleet fomation width*/
 float length_bound; /*<  Maximal fleet fomation length*/
 float height_bound; /*<  Maximal fleet fomation height*/
 uint16_t barycenter_alt; /*<  Fleet barycenter altitude (GPS)*/
 uint16_t barycenter_relative_alt; /*<  Fleet barycenter altitude from ground*/
 uint8_t formation_type; /*<  Fleet formation type (consult ENUM_FLEET_FORMATION_TYPE)*/
 uint8_t leader_ID; /*<  ID of the fleet leader*/
 uint8_t norm_type; /*<  Formation used norm type*/
}) mavlink_fleet_set_formation_t;

#define MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN 155
#define MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN 155
#define MAVLINK_MSG_ID_170_LEN 155
#define MAVLINK_MSG_ID_170_MIN_LEN 155

#define MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC 213
#define MAVLINK_MSG_ID_170_CRC 213



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_SET_FORMATION { \
    170, \
    "FLEET_SET_FORMATION", \
    26, \
    {  { "d", NULL, MAVLINK_TYPE_DOUBLE, 0, 0, offsetof(mavlink_fleet_set_formation_t, d) }, \
         { "epsilon", NULL, MAVLINK_TYPE_DOUBLE, 0, 8, offsetof(mavlink_fleet_set_formation_t, epsilon) }, \
         { "FoV", NULL, MAVLINK_TYPE_DOUBLE, 0, 16, offsetof(mavlink_fleet_set_formation_t, FoV) }, \
         { "AtractMax", NULL, MAVLINK_TYPE_DOUBLE, 0, 24, offsetof(mavlink_fleet_set_formation_t, AtractMax) }, \
         { "RepulsionMax", NULL, MAVLINK_TYPE_DOUBLE, 0, 32, offsetof(mavlink_fleet_set_formation_t, RepulsionMax) }, \
         { "h_", NULL, MAVLINK_TYPE_DOUBLE, 0, 40, offsetof(mavlink_fleet_set_formation_t, h_) }, \
         { "c1", NULL, MAVLINK_TYPE_DOUBLE, 0, 48, offsetof(mavlink_fleet_set_formation_t, c1) }, \
         { "c2", NULL, MAVLINK_TYPE_DOUBLE, 0, 56, offsetof(mavlink_fleet_set_formation_t, c2) }, \
         { "Kd_form", NULL, MAVLINK_TYPE_DOUBLE, 0, 64, offsetof(mavlink_fleet_set_formation_t, Kd_form) }, \
         { "Kp_form", NULL, MAVLINK_TYPE_DOUBLE, 0, 72, offsetof(mavlink_fleet_set_formation_t, Kp_form) }, \
         { "Ki_form", NULL, MAVLINK_TYPE_DOUBLE, 0, 80, offsetof(mavlink_fleet_set_formation_t, Ki_form) }, \
         { "T", NULL, MAVLINK_TYPE_DOUBLE, 0, 88, offsetof(mavlink_fleet_set_formation_t, T) }, \
         { "ux_sat", NULL, MAVLINK_TYPE_DOUBLE, 0, 96, offsetof(mavlink_fleet_set_formation_t, ux_sat) }, \
         { "uy_sat", NULL, MAVLINK_TYPE_DOUBLE, 0, 104, offsetof(mavlink_fleet_set_formation_t, uy_sat) }, \
         { "uz_sat", NULL, MAVLINK_TYPE_DOUBLE, 0, 112, offsetof(mavlink_fleet_set_formation_t, uz_sat) }, \
         { "Radius", NULL, MAVLINK_TYPE_DOUBLE, 0, 120, offsetof(mavlink_fleet_set_formation_t, Radius) }, \
         { "barycenter_lat", NULL, MAVLINK_TYPE_UINT32_T, 0, 128, offsetof(mavlink_fleet_set_formation_t, barycenter_lat) }, \
         { "barycenter_long", NULL, MAVLINK_TYPE_UINT32_T, 0, 132, offsetof(mavlink_fleet_set_formation_t, barycenter_long) }, \
         { "width_bound", NULL, MAVLINK_TYPE_FLOAT, 0, 136, offsetof(mavlink_fleet_set_formation_t, width_bound) }, \
         { "length_bound", NULL, MAVLINK_TYPE_FLOAT, 0, 140, offsetof(mavlink_fleet_set_formation_t, length_bound) }, \
         { "height_bound", NULL, MAVLINK_TYPE_FLOAT, 0, 144, offsetof(mavlink_fleet_set_formation_t, height_bound) }, \
         { "barycenter_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 148, offsetof(mavlink_fleet_set_formation_t, barycenter_alt) }, \
         { "barycenter_relative_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 150, offsetof(mavlink_fleet_set_formation_t, barycenter_relative_alt) }, \
         { "formation_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 152, offsetof(mavlink_fleet_set_formation_t, formation_type) }, \
         { "leader_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 153, offsetof(mavlink_fleet_set_formation_t, leader_ID) }, \
         { "norm_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 154, offsetof(mavlink_fleet_set_formation_t, norm_type) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_SET_FORMATION { \
    "FLEET_SET_FORMATION", \
    26, \
    {  { "d", NULL, MAVLINK_TYPE_DOUBLE, 0, 0, offsetof(mavlink_fleet_set_formation_t, d) }, \
         { "epsilon", NULL, MAVLINK_TYPE_DOUBLE, 0, 8, offsetof(mavlink_fleet_set_formation_t, epsilon) }, \
         { "FoV", NULL, MAVLINK_TYPE_DOUBLE, 0, 16, offsetof(mavlink_fleet_set_formation_t, FoV) }, \
         { "AtractMax", NULL, MAVLINK_TYPE_DOUBLE, 0, 24, offsetof(mavlink_fleet_set_formation_t, AtractMax) }, \
         { "RepulsionMax", NULL, MAVLINK_TYPE_DOUBLE, 0, 32, offsetof(mavlink_fleet_set_formation_t, RepulsionMax) }, \
         { "h_", NULL, MAVLINK_TYPE_DOUBLE, 0, 40, offsetof(mavlink_fleet_set_formation_t, h_) }, \
         { "c1", NULL, MAVLINK_TYPE_DOUBLE, 0, 48, offsetof(mavlink_fleet_set_formation_t, c1) }, \
         { "c2", NULL, MAVLINK_TYPE_DOUBLE, 0, 56, offsetof(mavlink_fleet_set_formation_t, c2) }, \
         { "Kd_form", NULL, MAVLINK_TYPE_DOUBLE, 0, 64, offsetof(mavlink_fleet_set_formation_t, Kd_form) }, \
         { "Kp_form", NULL, MAVLINK_TYPE_DOUBLE, 0, 72, offsetof(mavlink_fleet_set_formation_t, Kp_form) }, \
         { "Ki_form", NULL, MAVLINK_TYPE_DOUBLE, 0, 80, offsetof(mavlink_fleet_set_formation_t, Ki_form) }, \
         { "T", NULL, MAVLINK_TYPE_DOUBLE, 0, 88, offsetof(mavlink_fleet_set_formation_t, T) }, \
         { "ux_sat", NULL, MAVLINK_TYPE_DOUBLE, 0, 96, offsetof(mavlink_fleet_set_formation_t, ux_sat) }, \
         { "uy_sat", NULL, MAVLINK_TYPE_DOUBLE, 0, 104, offsetof(mavlink_fleet_set_formation_t, uy_sat) }, \
         { "uz_sat", NULL, MAVLINK_TYPE_DOUBLE, 0, 112, offsetof(mavlink_fleet_set_formation_t, uz_sat) }, \
         { "Radius", NULL, MAVLINK_TYPE_DOUBLE, 0, 120, offsetof(mavlink_fleet_set_formation_t, Radius) }, \
         { "barycenter_lat", NULL, MAVLINK_TYPE_UINT32_T, 0, 128, offsetof(mavlink_fleet_set_formation_t, barycenter_lat) }, \
         { "barycenter_long", NULL, MAVLINK_TYPE_UINT32_T, 0, 132, offsetof(mavlink_fleet_set_formation_t, barycenter_long) }, \
         { "width_bound", NULL, MAVLINK_TYPE_FLOAT, 0, 136, offsetof(mavlink_fleet_set_formation_t, width_bound) }, \
         { "length_bound", NULL, MAVLINK_TYPE_FLOAT, 0, 140, offsetof(mavlink_fleet_set_formation_t, length_bound) }, \
         { "height_bound", NULL, MAVLINK_TYPE_FLOAT, 0, 144, offsetof(mavlink_fleet_set_formation_t, height_bound) }, \
         { "barycenter_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 148, offsetof(mavlink_fleet_set_formation_t, barycenter_alt) }, \
         { "barycenter_relative_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 150, offsetof(mavlink_fleet_set_formation_t, barycenter_relative_alt) }, \
         { "formation_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 152, offsetof(mavlink_fleet_set_formation_t, formation_type) }, \
         { "leader_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 153, offsetof(mavlink_fleet_set_formation_t, leader_ID) }, \
         { "norm_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 154, offsetof(mavlink_fleet_set_formation_t, norm_type) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_set_formation message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param formation_type  Fleet formation type (consult ENUM_FLEET_FORMATION_TYPE)
 * @param leader_ID  ID of the fleet leader
 * @param barycenter_lat  Fleet barycenter latitude
 * @param barycenter_long  Fleet barycenter longitude
 * @param barycenter_alt  Fleet barycenter altitude (GPS)
 * @param barycenter_relative_alt  Fleet barycenter altitude from ground
 * @param width_bound  Maximal fleet fomation width
 * @param length_bound  Maximal fleet fomation length
 * @param height_bound  Maximal fleet fomation height
 * @param d  Security interdistace between UAVs
 * @param epsilon  sigma norm parameter
 * @param FoV  Field of view of a UAV
 * @param AtractMax  Maximal attraction force
 * @param RepulsionMax  Maximal repulsion force
 * @param h_  Potention function cuttoff parameter
 * @param c1  Navigation proportional gain
 * @param c2  Navigation derivative gain
 * @param Kd_form  Formation derivative gain
 * @param Kp_form  Formation proportional gain
 * @param Ki_form  Formation integral gain
 * @param T  user defined delta T
 * @param ux_sat  Formation x control output saturation
 * @param uy_sat  Formation y control output saturation
 * @param uz_sat  Formation z control output saturation
 * @param Radius  Circle radius
 * @param norm_type  Formation used norm type
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_set_formation_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t formation_type, uint8_t leader_ID, uint32_t barycenter_lat, uint32_t barycenter_long, uint16_t barycenter_alt, uint16_t barycenter_relative_alt, float width_bound, float length_bound, float height_bound, double d, double epsilon, double FoV, double AtractMax, double RepulsionMax, double h_, double c1, double c2, double Kd_form, double Kp_form, double Ki_form, double T, double ux_sat, double uy_sat, double uz_sat, double Radius, uint8_t norm_type)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN];
    _mav_put_double(buf, 0, d);
    _mav_put_double(buf, 8, epsilon);
    _mav_put_double(buf, 16, FoV);
    _mav_put_double(buf, 24, AtractMax);
    _mav_put_double(buf, 32, RepulsionMax);
    _mav_put_double(buf, 40, h_);
    _mav_put_double(buf, 48, c1);
    _mav_put_double(buf, 56, c2);
    _mav_put_double(buf, 64, Kd_form);
    _mav_put_double(buf, 72, Kp_form);
    _mav_put_double(buf, 80, Ki_form);
    _mav_put_double(buf, 88, T);
    _mav_put_double(buf, 96, ux_sat);
    _mav_put_double(buf, 104, uy_sat);
    _mav_put_double(buf, 112, uz_sat);
    _mav_put_double(buf, 120, Radius);
    _mav_put_uint32_t(buf, 128, barycenter_lat);
    _mav_put_uint32_t(buf, 132, barycenter_long);
    _mav_put_float(buf, 136, width_bound);
    _mav_put_float(buf, 140, length_bound);
    _mav_put_float(buf, 144, height_bound);
    _mav_put_uint16_t(buf, 148, barycenter_alt);
    _mav_put_uint16_t(buf, 150, barycenter_relative_alt);
    _mav_put_uint8_t(buf, 152, formation_type);
    _mav_put_uint8_t(buf, 153, leader_ID);
    _mav_put_uint8_t(buf, 154, norm_type);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN);
#else
    mavlink_fleet_set_formation_t packet;
    packet.d = d;
    packet.epsilon = epsilon;
    packet.FoV = FoV;
    packet.AtractMax = AtractMax;
    packet.RepulsionMax = RepulsionMax;
    packet.h_ = h_;
    packet.c1 = c1;
    packet.c2 = c2;
    packet.Kd_form = Kd_form;
    packet.Kp_form = Kp_form;
    packet.Ki_form = Ki_form;
    packet.T = T;
    packet.ux_sat = ux_sat;
    packet.uy_sat = uy_sat;
    packet.uz_sat = uz_sat;
    packet.Radius = Radius;
    packet.barycenter_lat = barycenter_lat;
    packet.barycenter_long = barycenter_long;
    packet.width_bound = width_bound;
    packet.length_bound = length_bound;
    packet.height_bound = height_bound;
    packet.barycenter_alt = barycenter_alt;
    packet.barycenter_relative_alt = barycenter_relative_alt;
    packet.formation_type = formation_type;
    packet.leader_ID = leader_ID;
    packet.norm_type = norm_type;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_SET_FORMATION;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC);
}

/**
 * @brief Pack a fleet_set_formation message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param formation_type  Fleet formation type (consult ENUM_FLEET_FORMATION_TYPE)
 * @param leader_ID  ID of the fleet leader
 * @param barycenter_lat  Fleet barycenter latitude
 * @param barycenter_long  Fleet barycenter longitude
 * @param barycenter_alt  Fleet barycenter altitude (GPS)
 * @param barycenter_relative_alt  Fleet barycenter altitude from ground
 * @param width_bound  Maximal fleet fomation width
 * @param length_bound  Maximal fleet fomation length
 * @param height_bound  Maximal fleet fomation height
 * @param d  Security interdistace between UAVs
 * @param epsilon  sigma norm parameter
 * @param FoV  Field of view of a UAV
 * @param AtractMax  Maximal attraction force
 * @param RepulsionMax  Maximal repulsion force
 * @param h_  Potention function cuttoff parameter
 * @param c1  Navigation proportional gain
 * @param c2  Navigation derivative gain
 * @param Kd_form  Formation derivative gain
 * @param Kp_form  Formation proportional gain
 * @param Ki_form  Formation integral gain
 * @param T  user defined delta T
 * @param ux_sat  Formation x control output saturation
 * @param uy_sat  Formation y control output saturation
 * @param uz_sat  Formation z control output saturation
 * @param Radius  Circle radius
 * @param norm_type  Formation used norm type
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_set_formation_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t formation_type,uint8_t leader_ID,uint32_t barycenter_lat,uint32_t barycenter_long,uint16_t barycenter_alt,uint16_t barycenter_relative_alt,float width_bound,float length_bound,float height_bound,double d,double epsilon,double FoV,double AtractMax,double RepulsionMax,double h_,double c1,double c2,double Kd_form,double Kp_form,double Ki_form,double T,double ux_sat,double uy_sat,double uz_sat,double Radius,uint8_t norm_type)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN];
    _mav_put_double(buf, 0, d);
    _mav_put_double(buf, 8, epsilon);
    _mav_put_double(buf, 16, FoV);
    _mav_put_double(buf, 24, AtractMax);
    _mav_put_double(buf, 32, RepulsionMax);
    _mav_put_double(buf, 40, h_);
    _mav_put_double(buf, 48, c1);
    _mav_put_double(buf, 56, c2);
    _mav_put_double(buf, 64, Kd_form);
    _mav_put_double(buf, 72, Kp_form);
    _mav_put_double(buf, 80, Ki_form);
    _mav_put_double(buf, 88, T);
    _mav_put_double(buf, 96, ux_sat);
    _mav_put_double(buf, 104, uy_sat);
    _mav_put_double(buf, 112, uz_sat);
    _mav_put_double(buf, 120, Radius);
    _mav_put_uint32_t(buf, 128, barycenter_lat);
    _mav_put_uint32_t(buf, 132, barycenter_long);
    _mav_put_float(buf, 136, width_bound);
    _mav_put_float(buf, 140, length_bound);
    _mav_put_float(buf, 144, height_bound);
    _mav_put_uint16_t(buf, 148, barycenter_alt);
    _mav_put_uint16_t(buf, 150, barycenter_relative_alt);
    _mav_put_uint8_t(buf, 152, formation_type);
    _mav_put_uint8_t(buf, 153, leader_ID);
    _mav_put_uint8_t(buf, 154, norm_type);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN);
#else
    mavlink_fleet_set_formation_t packet;
    packet.d = d;
    packet.epsilon = epsilon;
    packet.FoV = FoV;
    packet.AtractMax = AtractMax;
    packet.RepulsionMax = RepulsionMax;
    packet.h_ = h_;
    packet.c1 = c1;
    packet.c2 = c2;
    packet.Kd_form = Kd_form;
    packet.Kp_form = Kp_form;
    packet.Ki_form = Ki_form;
    packet.T = T;
    packet.ux_sat = ux_sat;
    packet.uy_sat = uy_sat;
    packet.uz_sat = uz_sat;
    packet.Radius = Radius;
    packet.barycenter_lat = barycenter_lat;
    packet.barycenter_long = barycenter_long;
    packet.width_bound = width_bound;
    packet.length_bound = length_bound;
    packet.height_bound = height_bound;
    packet.barycenter_alt = barycenter_alt;
    packet.barycenter_relative_alt = barycenter_relative_alt;
    packet.formation_type = formation_type;
    packet.leader_ID = leader_ID;
    packet.norm_type = norm_type;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_SET_FORMATION;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC);
}

/**
 * @brief Encode a fleet_set_formation struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_set_formation C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_set_formation_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_set_formation_t* fleet_set_formation)
{
    return mavlink_msg_fleet_set_formation_pack(system_id, component_id, msg, fleet_set_formation->formation_type, fleet_set_formation->leader_ID, fleet_set_formation->barycenter_lat, fleet_set_formation->barycenter_long, fleet_set_formation->barycenter_alt, fleet_set_formation->barycenter_relative_alt, fleet_set_formation->width_bound, fleet_set_formation->length_bound, fleet_set_formation->height_bound, fleet_set_formation->d, fleet_set_formation->epsilon, fleet_set_formation->FoV, fleet_set_formation->AtractMax, fleet_set_formation->RepulsionMax, fleet_set_formation->h_, fleet_set_formation->c1, fleet_set_formation->c2, fleet_set_formation->Kd_form, fleet_set_formation->Kp_form, fleet_set_formation->Ki_form, fleet_set_formation->T, fleet_set_formation->ux_sat, fleet_set_formation->uy_sat, fleet_set_formation->uz_sat, fleet_set_formation->Radius, fleet_set_formation->norm_type);
}

/**
 * @brief Encode a fleet_set_formation struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_set_formation C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_set_formation_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_set_formation_t* fleet_set_formation)
{
    return mavlink_msg_fleet_set_formation_pack_chan(system_id, component_id, chan, msg, fleet_set_formation->formation_type, fleet_set_formation->leader_ID, fleet_set_formation->barycenter_lat, fleet_set_formation->barycenter_long, fleet_set_formation->barycenter_alt, fleet_set_formation->barycenter_relative_alt, fleet_set_formation->width_bound, fleet_set_formation->length_bound, fleet_set_formation->height_bound, fleet_set_formation->d, fleet_set_formation->epsilon, fleet_set_formation->FoV, fleet_set_formation->AtractMax, fleet_set_formation->RepulsionMax, fleet_set_formation->h_, fleet_set_formation->c1, fleet_set_formation->c2, fleet_set_formation->Kd_form, fleet_set_formation->Kp_form, fleet_set_formation->Ki_form, fleet_set_formation->T, fleet_set_formation->ux_sat, fleet_set_formation->uy_sat, fleet_set_formation->uz_sat, fleet_set_formation->Radius, fleet_set_formation->norm_type);
}

/**
 * @brief Send a fleet_set_formation message
 * @param chan MAVLink channel to send the message
 *
 * @param formation_type  Fleet formation type (consult ENUM_FLEET_FORMATION_TYPE)
 * @param leader_ID  ID of the fleet leader
 * @param barycenter_lat  Fleet barycenter latitude
 * @param barycenter_long  Fleet barycenter longitude
 * @param barycenter_alt  Fleet barycenter altitude (GPS)
 * @param barycenter_relative_alt  Fleet barycenter altitude from ground
 * @param width_bound  Maximal fleet fomation width
 * @param length_bound  Maximal fleet fomation length
 * @param height_bound  Maximal fleet fomation height
 * @param d  Security interdistace between UAVs
 * @param epsilon  sigma norm parameter
 * @param FoV  Field of view of a UAV
 * @param AtractMax  Maximal attraction force
 * @param RepulsionMax  Maximal repulsion force
 * @param h_  Potention function cuttoff parameter
 * @param c1  Navigation proportional gain
 * @param c2  Navigation derivative gain
 * @param Kd_form  Formation derivative gain
 * @param Kp_form  Formation proportional gain
 * @param Ki_form  Formation integral gain
 * @param T  user defined delta T
 * @param ux_sat  Formation x control output saturation
 * @param uy_sat  Formation y control output saturation
 * @param uz_sat  Formation z control output saturation
 * @param Radius  Circle radius
 * @param norm_type  Formation used norm type
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_set_formation_send(mavlink_channel_t chan, uint8_t formation_type, uint8_t leader_ID, uint32_t barycenter_lat, uint32_t barycenter_long, uint16_t barycenter_alt, uint16_t barycenter_relative_alt, float width_bound, float length_bound, float height_bound, double d, double epsilon, double FoV, double AtractMax, double RepulsionMax, double h_, double c1, double c2, double Kd_form, double Kp_form, double Ki_form, double T, double ux_sat, double uy_sat, double uz_sat, double Radius, uint8_t norm_type)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN];
    _mav_put_double(buf, 0, d);
    _mav_put_double(buf, 8, epsilon);
    _mav_put_double(buf, 16, FoV);
    _mav_put_double(buf, 24, AtractMax);
    _mav_put_double(buf, 32, RepulsionMax);
    _mav_put_double(buf, 40, h_);
    _mav_put_double(buf, 48, c1);
    _mav_put_double(buf, 56, c2);
    _mav_put_double(buf, 64, Kd_form);
    _mav_put_double(buf, 72, Kp_form);
    _mav_put_double(buf, 80, Ki_form);
    _mav_put_double(buf, 88, T);
    _mav_put_double(buf, 96, ux_sat);
    _mav_put_double(buf, 104, uy_sat);
    _mav_put_double(buf, 112, uz_sat);
    _mav_put_double(buf, 120, Radius);
    _mav_put_uint32_t(buf, 128, barycenter_lat);
    _mav_put_uint32_t(buf, 132, barycenter_long);
    _mav_put_float(buf, 136, width_bound);
    _mav_put_float(buf, 140, length_bound);
    _mav_put_float(buf, 144, height_bound);
    _mav_put_uint16_t(buf, 148, barycenter_alt);
    _mav_put_uint16_t(buf, 150, barycenter_relative_alt);
    _mav_put_uint8_t(buf, 152, formation_type);
    _mav_put_uint8_t(buf, 153, leader_ID);
    _mav_put_uint8_t(buf, 154, norm_type);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_FORMATION, buf, MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC);
#else
    mavlink_fleet_set_formation_t packet;
    packet.d = d;
    packet.epsilon = epsilon;
    packet.FoV = FoV;
    packet.AtractMax = AtractMax;
    packet.RepulsionMax = RepulsionMax;
    packet.h_ = h_;
    packet.c1 = c1;
    packet.c2 = c2;
    packet.Kd_form = Kd_form;
    packet.Kp_form = Kp_form;
    packet.Ki_form = Ki_form;
    packet.T = T;
    packet.ux_sat = ux_sat;
    packet.uy_sat = uy_sat;
    packet.uz_sat = uz_sat;
    packet.Radius = Radius;
    packet.barycenter_lat = barycenter_lat;
    packet.barycenter_long = barycenter_long;
    packet.width_bound = width_bound;
    packet.length_bound = length_bound;
    packet.height_bound = height_bound;
    packet.barycenter_alt = barycenter_alt;
    packet.barycenter_relative_alt = barycenter_relative_alt;
    packet.formation_type = formation_type;
    packet.leader_ID = leader_ID;
    packet.norm_type = norm_type;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_FORMATION, (const char *)&packet, MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC);
#endif
}

/**
 * @brief Send a fleet_set_formation message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_set_formation_send_struct(mavlink_channel_t chan, const mavlink_fleet_set_formation_t* fleet_set_formation)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_set_formation_send(chan, fleet_set_formation->formation_type, fleet_set_formation->leader_ID, fleet_set_formation->barycenter_lat, fleet_set_formation->barycenter_long, fleet_set_formation->barycenter_alt, fleet_set_formation->barycenter_relative_alt, fleet_set_formation->width_bound, fleet_set_formation->length_bound, fleet_set_formation->height_bound, fleet_set_formation->d, fleet_set_formation->epsilon, fleet_set_formation->FoV, fleet_set_formation->AtractMax, fleet_set_formation->RepulsionMax, fleet_set_formation->h_, fleet_set_formation->c1, fleet_set_formation->c2, fleet_set_formation->Kd_form, fleet_set_formation->Kp_form, fleet_set_formation->Ki_form, fleet_set_formation->T, fleet_set_formation->ux_sat, fleet_set_formation->uy_sat, fleet_set_formation->uz_sat, fleet_set_formation->Radius, fleet_set_formation->norm_type);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_FORMATION, (const char *)fleet_set_formation, MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_set_formation_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t formation_type, uint8_t leader_ID, uint32_t barycenter_lat, uint32_t barycenter_long, uint16_t barycenter_alt, uint16_t barycenter_relative_alt, float width_bound, float length_bound, float height_bound, double d, double epsilon, double FoV, double AtractMax, double RepulsionMax, double h_, double c1, double c2, double Kd_form, double Kp_form, double Ki_form, double T, double ux_sat, double uy_sat, double uz_sat, double Radius, uint8_t norm_type)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_double(buf, 0, d);
    _mav_put_double(buf, 8, epsilon);
    _mav_put_double(buf, 16, FoV);
    _mav_put_double(buf, 24, AtractMax);
    _mav_put_double(buf, 32, RepulsionMax);
    _mav_put_double(buf, 40, h_);
    _mav_put_double(buf, 48, c1);
    _mav_put_double(buf, 56, c2);
    _mav_put_double(buf, 64, Kd_form);
    _mav_put_double(buf, 72, Kp_form);
    _mav_put_double(buf, 80, Ki_form);
    _mav_put_double(buf, 88, T);
    _mav_put_double(buf, 96, ux_sat);
    _mav_put_double(buf, 104, uy_sat);
    _mav_put_double(buf, 112, uz_sat);
    _mav_put_double(buf, 120, Radius);
    _mav_put_uint32_t(buf, 128, barycenter_lat);
    _mav_put_uint32_t(buf, 132, barycenter_long);
    _mav_put_float(buf, 136, width_bound);
    _mav_put_float(buf, 140, length_bound);
    _mav_put_float(buf, 144, height_bound);
    _mav_put_uint16_t(buf, 148, barycenter_alt);
    _mav_put_uint16_t(buf, 150, barycenter_relative_alt);
    _mav_put_uint8_t(buf, 152, formation_type);
    _mav_put_uint8_t(buf, 153, leader_ID);
    _mav_put_uint8_t(buf, 154, norm_type);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_FORMATION, buf, MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC);
#else
    mavlink_fleet_set_formation_t *packet = (mavlink_fleet_set_formation_t *)msgbuf;
    packet->d = d;
    packet->epsilon = epsilon;
    packet->FoV = FoV;
    packet->AtractMax = AtractMax;
    packet->RepulsionMax = RepulsionMax;
    packet->h_ = h_;
    packet->c1 = c1;
    packet->c2 = c2;
    packet->Kd_form = Kd_form;
    packet->Kp_form = Kp_form;
    packet->Ki_form = Ki_form;
    packet->T = T;
    packet->ux_sat = ux_sat;
    packet->uy_sat = uy_sat;
    packet->uz_sat = uz_sat;
    packet->Radius = Radius;
    packet->barycenter_lat = barycenter_lat;
    packet->barycenter_long = barycenter_long;
    packet->width_bound = width_bound;
    packet->length_bound = length_bound;
    packet->height_bound = height_bound;
    packet->barycenter_alt = barycenter_alt;
    packet->barycenter_relative_alt = barycenter_relative_alt;
    packet->formation_type = formation_type;
    packet->leader_ID = leader_ID;
    packet->norm_type = norm_type;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_FORMATION, (const char *)packet, MAVLINK_MSG_ID_FLEET_SET_FORMATION_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN, MAVLINK_MSG_ID_FLEET_SET_FORMATION_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_SET_FORMATION UNPACKING


/**
 * @brief Get field formation_type from fleet_set_formation message
 *
 * @return  Fleet formation type (consult ENUM_FLEET_FORMATION_TYPE)
 */
static inline uint8_t mavlink_msg_fleet_set_formation_get_formation_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  152);
}

/**
 * @brief Get field leader_ID from fleet_set_formation message
 *
 * @return  ID of the fleet leader
 */
static inline uint8_t mavlink_msg_fleet_set_formation_get_leader_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  153);
}

/**
 * @brief Get field barycenter_lat from fleet_set_formation message
 *
 * @return  Fleet barycenter latitude
 */
static inline uint32_t mavlink_msg_fleet_set_formation_get_barycenter_lat(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  128);
}

/**
 * @brief Get field barycenter_long from fleet_set_formation message
 *
 * @return  Fleet barycenter longitude
 */
static inline uint32_t mavlink_msg_fleet_set_formation_get_barycenter_long(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  132);
}

/**
 * @brief Get field barycenter_alt from fleet_set_formation message
 *
 * @return  Fleet barycenter altitude (GPS)
 */
static inline uint16_t mavlink_msg_fleet_set_formation_get_barycenter_alt(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  148);
}

/**
 * @brief Get field barycenter_relative_alt from fleet_set_formation message
 *
 * @return  Fleet barycenter altitude from ground
 */
static inline uint16_t mavlink_msg_fleet_set_formation_get_barycenter_relative_alt(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  150);
}

/**
 * @brief Get field width_bound from fleet_set_formation message
 *
 * @return  Maximal fleet fomation width
 */
static inline float mavlink_msg_fleet_set_formation_get_width_bound(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  136);
}

/**
 * @brief Get field length_bound from fleet_set_formation message
 *
 * @return  Maximal fleet fomation length
 */
static inline float mavlink_msg_fleet_set_formation_get_length_bound(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  140);
}

/**
 * @brief Get field height_bound from fleet_set_formation message
 *
 * @return  Maximal fleet fomation height
 */
static inline float mavlink_msg_fleet_set_formation_get_height_bound(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  144);
}

/**
 * @brief Get field d from fleet_set_formation message
 *
 * @return  Security interdistace between UAVs
 */
static inline double mavlink_msg_fleet_set_formation_get_d(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  0);
}

/**
 * @brief Get field epsilon from fleet_set_formation message
 *
 * @return  sigma norm parameter
 */
static inline double mavlink_msg_fleet_set_formation_get_epsilon(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  8);
}

/**
 * @brief Get field FoV from fleet_set_formation message
 *
 * @return  Field of view of a UAV
 */
static inline double mavlink_msg_fleet_set_formation_get_FoV(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  16);
}

/**
 * @brief Get field AtractMax from fleet_set_formation message
 *
 * @return  Maximal attraction force
 */
static inline double mavlink_msg_fleet_set_formation_get_AtractMax(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  24);
}

/**
 * @brief Get field RepulsionMax from fleet_set_formation message
 *
 * @return  Maximal repulsion force
 */
static inline double mavlink_msg_fleet_set_formation_get_RepulsionMax(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  32);
}

/**
 * @brief Get field h_ from fleet_set_formation message
 *
 * @return  Potention function cuttoff parameter
 */
static inline double mavlink_msg_fleet_set_formation_get_h_(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  40);
}

/**
 * @brief Get field c1 from fleet_set_formation message
 *
 * @return  Navigation proportional gain
 */
static inline double mavlink_msg_fleet_set_formation_get_c1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  48);
}

/**
 * @brief Get field c2 from fleet_set_formation message
 *
 * @return  Navigation derivative gain
 */
static inline double mavlink_msg_fleet_set_formation_get_c2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  56);
}

/**
 * @brief Get field Kd_form from fleet_set_formation message
 *
 * @return  Formation derivative gain
 */
static inline double mavlink_msg_fleet_set_formation_get_Kd_form(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  64);
}

/**
 * @brief Get field Kp_form from fleet_set_formation message
 *
 * @return  Formation proportional gain
 */
static inline double mavlink_msg_fleet_set_formation_get_Kp_form(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  72);
}

/**
 * @brief Get field Ki_form from fleet_set_formation message
 *
 * @return  Formation integral gain
 */
static inline double mavlink_msg_fleet_set_formation_get_Ki_form(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  80);
}

/**
 * @brief Get field T from fleet_set_formation message
 *
 * @return  user defined delta T
 */
static inline double mavlink_msg_fleet_set_formation_get_T(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  88);
}

/**
 * @brief Get field ux_sat from fleet_set_formation message
 *
 * @return  Formation x control output saturation
 */
static inline double mavlink_msg_fleet_set_formation_get_ux_sat(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  96);
}

/**
 * @brief Get field uy_sat from fleet_set_formation message
 *
 * @return  Formation y control output saturation
 */
static inline double mavlink_msg_fleet_set_formation_get_uy_sat(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  104);
}

/**
 * @brief Get field uz_sat from fleet_set_formation message
 *
 * @return  Formation z control output saturation
 */
static inline double mavlink_msg_fleet_set_formation_get_uz_sat(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  112);
}

/**
 * @brief Get field Radius from fleet_set_formation message
 *
 * @return  Circle radius
 */
static inline double mavlink_msg_fleet_set_formation_get_Radius(const mavlink_message_t* msg)
{
    return _MAV_RETURN_double(msg,  120);
}

/**
 * @brief Get field norm_type from fleet_set_formation message
 *
 * @return  Formation used norm type
 */
static inline uint8_t mavlink_msg_fleet_set_formation_get_norm_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  154);
}

/**
 * @brief Decode a fleet_set_formation message into a struct
 *
 * @param msg The message to decode
 * @param fleet_set_formation C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_set_formation_decode(const mavlink_message_t* msg, mavlink_fleet_set_formation_t* fleet_set_formation)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_set_formation->d = mavlink_msg_fleet_set_formation_get_d(msg);
    fleet_set_formation->epsilon = mavlink_msg_fleet_set_formation_get_epsilon(msg);
    fleet_set_formation->FoV = mavlink_msg_fleet_set_formation_get_FoV(msg);
    fleet_set_formation->AtractMax = mavlink_msg_fleet_set_formation_get_AtractMax(msg);
    fleet_set_formation->RepulsionMax = mavlink_msg_fleet_set_formation_get_RepulsionMax(msg);
    fleet_set_formation->h_ = mavlink_msg_fleet_set_formation_get_h_(msg);
    fleet_set_formation->c1 = mavlink_msg_fleet_set_formation_get_c1(msg);
    fleet_set_formation->c2 = mavlink_msg_fleet_set_formation_get_c2(msg);
    fleet_set_formation->Kd_form = mavlink_msg_fleet_set_formation_get_Kd_form(msg);
    fleet_set_formation->Kp_form = mavlink_msg_fleet_set_formation_get_Kp_form(msg);
    fleet_set_formation->Ki_form = mavlink_msg_fleet_set_formation_get_Ki_form(msg);
    fleet_set_formation->T = mavlink_msg_fleet_set_formation_get_T(msg);
    fleet_set_formation->ux_sat = mavlink_msg_fleet_set_formation_get_ux_sat(msg);
    fleet_set_formation->uy_sat = mavlink_msg_fleet_set_formation_get_uy_sat(msg);
    fleet_set_formation->uz_sat = mavlink_msg_fleet_set_formation_get_uz_sat(msg);
    fleet_set_formation->Radius = mavlink_msg_fleet_set_formation_get_Radius(msg);
    fleet_set_formation->barycenter_lat = mavlink_msg_fleet_set_formation_get_barycenter_lat(msg);
    fleet_set_formation->barycenter_long = mavlink_msg_fleet_set_formation_get_barycenter_long(msg);
    fleet_set_formation->width_bound = mavlink_msg_fleet_set_formation_get_width_bound(msg);
    fleet_set_formation->length_bound = mavlink_msg_fleet_set_formation_get_length_bound(msg);
    fleet_set_formation->height_bound = mavlink_msg_fleet_set_formation_get_height_bound(msg);
    fleet_set_formation->barycenter_alt = mavlink_msg_fleet_set_formation_get_barycenter_alt(msg);
    fleet_set_formation->barycenter_relative_alt = mavlink_msg_fleet_set_formation_get_barycenter_relative_alt(msg);
    fleet_set_formation->formation_type = mavlink_msg_fleet_set_formation_get_formation_type(msg);
    fleet_set_formation->leader_ID = mavlink_msg_fleet_set_formation_get_leader_ID(msg);
    fleet_set_formation->norm_type = mavlink_msg_fleet_set_formation_get_norm_type(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN? msg->len : MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN;
        memset(fleet_set_formation, 0, MAVLINK_MSG_ID_FLEET_SET_FORMATION_LEN);
    memcpy(fleet_set_formation, _MAV_PAYLOAD(msg), len);
#endif
}
