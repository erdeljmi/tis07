#pragma once
// MESSAGE FLEET_UAV_ELECTED_ACCEPT PACKING

#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT 161

MAVPACKED(
typedef struct __mavlink_fleet_uav_elected_accept_t {
 uint8_t request_ID; /*<  Request's unique ID*/
}) mavlink_fleet_uav_elected_accept_t;

#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN 1
#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN 1
#define MAVLINK_MSG_ID_161_LEN 1
#define MAVLINK_MSG_ID_161_MIN_LEN 1

#define MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC 189
#define MAVLINK_MSG_ID_161_CRC 189



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_ELECTED_ACCEPT { \
    161, \
    "FLEET_UAV_ELECTED_ACCEPT", \
    1, \
    {  { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_uav_elected_accept_t, request_ID) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_ELECTED_ACCEPT { \
    "FLEET_UAV_ELECTED_ACCEPT", \
    1, \
    {  { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_fleet_uav_elected_accept_t, request_ID) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_uav_elected_accept message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param request_ID  Request's unique ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_accept_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN);
#else
    mavlink_fleet_uav_elected_accept_t packet;
    packet.request_ID = request_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC);
}

/**
 * @brief Pack a fleet_uav_elected_accept message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param request_ID  Request's unique ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_accept_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN);
#else
    mavlink_fleet_uav_elected_accept_t packet;
    packet.request_ID = request_ID;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC);
}

/**
 * @brief Encode a fleet_uav_elected_accept struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_elected_accept C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_accept_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_uav_elected_accept_t* fleet_uav_elected_accept)
{
    return mavlink_msg_fleet_uav_elected_accept_pack(system_id, component_id, msg, fleet_uav_elected_accept->request_ID);
}

/**
 * @brief Encode a fleet_uav_elected_accept struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_elected_accept C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_elected_accept_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_uav_elected_accept_t* fleet_uav_elected_accept)
{
    return mavlink_msg_fleet_uav_elected_accept_pack_chan(system_id, component_id, chan, msg, fleet_uav_elected_accept->request_ID);
}

/**
 * @brief Send a fleet_uav_elected_accept message
 * @param chan MAVLink channel to send the message
 *
 * @param request_ID  Request's unique ID
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_uav_elected_accept_send(mavlink_channel_t chan, uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN];
    _mav_put_uint8_t(buf, 0, request_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT, buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC);
#else
    mavlink_fleet_uav_elected_accept_t packet;
    packet.request_ID = request_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT, (const char *)&packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC);
#endif
}

/**
 * @brief Send a fleet_uav_elected_accept message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_uav_elected_accept_send_struct(mavlink_channel_t chan, const mavlink_fleet_uav_elected_accept_t* fleet_uav_elected_accept)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_uav_elected_accept_send(chan, fleet_uav_elected_accept->request_ID);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT, (const char *)fleet_uav_elected_accept, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_uav_elected_accept_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t request_ID)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, request_ID);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT, buf, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC);
#else
    mavlink_fleet_uav_elected_accept_t *packet = (mavlink_fleet_uav_elected_accept_t *)msgbuf;
    packet->request_ID = request_ID;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT, (const char *)packet, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_UAV_ELECTED_ACCEPT UNPACKING


/**
 * @brief Get field request_ID from fleet_uav_elected_accept message
 *
 * @return  Request's unique ID
 */
static inline uint8_t mavlink_msg_fleet_uav_elected_accept_get_request_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Decode a fleet_uav_elected_accept message into a struct
 *
 * @param msg The message to decode
 * @param fleet_uav_elected_accept C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_uav_elected_accept_decode(const mavlink_message_t* msg, mavlink_fleet_uav_elected_accept_t* fleet_uav_elected_accept)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_uav_elected_accept->request_ID = mavlink_msg_fleet_uav_elected_accept_get_request_ID(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN? msg->len : MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN;
        memset(fleet_uav_elected_accept, 0, MAVLINK_MSG_ID_FLEET_UAV_ELECTED_ACCEPT_LEN);
    memcpy(fleet_uav_elected_accept, _MAV_PAYLOAD(msg), len);
#endif
}
