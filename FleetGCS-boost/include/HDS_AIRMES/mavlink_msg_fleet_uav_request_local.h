#pragma once
// MESSAGE FLEET_UAV_REQUEST_LOCAL PACKING

#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL 152

MAVPACKED(
typedef struct __mavlink_fleet_uav_request_local_t {
 float x; /*<  Coordinate X*/
 float y; /*<  Coordinate Y*/
 float z; /*<  Coordinate Z*/
 uint8_t request_type; /*<  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)*/
 uint8_t request_ID; /*<  Request's unique ID*/
 uint8_t ID_to_replace; /*<  System ID of the system/component that sent the request*/
 uint8_t request_urgency; /*<  Parameter used as a coefficient for choosing the most critical request to respond to*/
}) mavlink_fleet_uav_request_local_t;

#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN 16
#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN 16
#define MAVLINK_MSG_ID_152_LEN 16
#define MAVLINK_MSG_ID_152_MIN_LEN 16

#define MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC 228
#define MAVLINK_MSG_ID_152_CRC 228



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_REQUEST_LOCAL { \
    152, \
    "FLEET_UAV_REQUEST_LOCAL", \
    7, \
    {  { "x", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_fleet_uav_request_local_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_fleet_uav_request_local_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_fleet_uav_request_local_t, z) }, \
         { "request_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 12, offsetof(mavlink_fleet_uav_request_local_t, request_type) }, \
         { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 13, offsetof(mavlink_fleet_uav_request_local_t, request_ID) }, \
         { "ID_to_replace", NULL, MAVLINK_TYPE_UINT8_T, 0, 14, offsetof(mavlink_fleet_uav_request_local_t, ID_to_replace) }, \
         { "request_urgency", NULL, MAVLINK_TYPE_UINT8_T, 0, 15, offsetof(mavlink_fleet_uav_request_local_t, request_urgency) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_UAV_REQUEST_LOCAL { \
    "FLEET_UAV_REQUEST_LOCAL", \
    7, \
    {  { "x", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_fleet_uav_request_local_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_fleet_uav_request_local_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_fleet_uav_request_local_t, z) }, \
         { "request_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 12, offsetof(mavlink_fleet_uav_request_local_t, request_type) }, \
         { "request_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 13, offsetof(mavlink_fleet_uav_request_local_t, request_ID) }, \
         { "ID_to_replace", NULL, MAVLINK_TYPE_UINT8_T, 0, 14, offsetof(mavlink_fleet_uav_request_local_t, ID_to_replace) }, \
         { "request_urgency", NULL, MAVLINK_TYPE_UINT8_T, 0, 15, offsetof(mavlink_fleet_uav_request_local_t, request_urgency) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_uav_request_local message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param request_type  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 * @param request_ID  Request's unique ID
 * @param ID_to_replace  System ID of the system/component that sent the request
 * @param x  Coordinate X
 * @param y  Coordinate Y
 * @param z  Coordinate Z
 * @param request_urgency  Parameter used as a coefficient for choosing the most critical request to respond to
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_request_local_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t request_type, uint8_t request_ID, uint8_t ID_to_replace, float x, float y, float z, uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN];
    _mav_put_float(buf, 0, x);
    _mav_put_float(buf, 4, y);
    _mav_put_float(buf, 8, z);
    _mav_put_uint8_t(buf, 12, request_type);
    _mav_put_uint8_t(buf, 13, request_ID);
    _mav_put_uint8_t(buf, 14, ID_to_replace);
    _mav_put_uint8_t(buf, 15, request_urgency);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN);
#else
    mavlink_fleet_uav_request_local_t packet;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.request_type = request_type;
    packet.request_ID = request_ID;
    packet.ID_to_replace = ID_to_replace;
    packet.request_urgency = request_urgency;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC);
}

/**
 * @brief Pack a fleet_uav_request_local message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param request_type  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 * @param request_ID  Request's unique ID
 * @param ID_to_replace  System ID of the system/component that sent the request
 * @param x  Coordinate X
 * @param y  Coordinate Y
 * @param z  Coordinate Z
 * @param request_urgency  Parameter used as a coefficient for choosing the most critical request to respond to
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_uav_request_local_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t request_type,uint8_t request_ID,uint8_t ID_to_replace,float x,float y,float z,uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN];
    _mav_put_float(buf, 0, x);
    _mav_put_float(buf, 4, y);
    _mav_put_float(buf, 8, z);
    _mav_put_uint8_t(buf, 12, request_type);
    _mav_put_uint8_t(buf, 13, request_ID);
    _mav_put_uint8_t(buf, 14, ID_to_replace);
    _mav_put_uint8_t(buf, 15, request_urgency);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN);
#else
    mavlink_fleet_uav_request_local_t packet;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.request_type = request_type;
    packet.request_ID = request_ID;
    packet.ID_to_replace = ID_to_replace;
    packet.request_urgency = request_urgency;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC);
}

/**
 * @brief Encode a fleet_uav_request_local struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_request_local C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_request_local_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_uav_request_local_t* fleet_uav_request_local)
{
    return mavlink_msg_fleet_uav_request_local_pack(system_id, component_id, msg, fleet_uav_request_local->request_type, fleet_uav_request_local->request_ID, fleet_uav_request_local->ID_to_replace, fleet_uav_request_local->x, fleet_uav_request_local->y, fleet_uav_request_local->z, fleet_uav_request_local->request_urgency);
}

/**
 * @brief Encode a fleet_uav_request_local struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_uav_request_local C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_uav_request_local_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_uav_request_local_t* fleet_uav_request_local)
{
    return mavlink_msg_fleet_uav_request_local_pack_chan(system_id, component_id, chan, msg, fleet_uav_request_local->request_type, fleet_uav_request_local->request_ID, fleet_uav_request_local->ID_to_replace, fleet_uav_request_local->x, fleet_uav_request_local->y, fleet_uav_request_local->z, fleet_uav_request_local->request_urgency);
}

/**
 * @brief Send a fleet_uav_request_local message
 * @param chan MAVLink channel to send the message
 *
 * @param request_type  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 * @param request_ID  Request's unique ID
 * @param ID_to_replace  System ID of the system/component that sent the request
 * @param x  Coordinate X
 * @param y  Coordinate Y
 * @param z  Coordinate Z
 * @param request_urgency  Parameter used as a coefficient for choosing the most critical request to respond to
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_uav_request_local_send(mavlink_channel_t chan, uint8_t request_type, uint8_t request_ID, uint8_t ID_to_replace, float x, float y, float z, uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN];
    _mav_put_float(buf, 0, x);
    _mav_put_float(buf, 4, y);
    _mav_put_float(buf, 8, z);
    _mav_put_uint8_t(buf, 12, request_type);
    _mav_put_uint8_t(buf, 13, request_ID);
    _mav_put_uint8_t(buf, 14, ID_to_replace);
    _mav_put_uint8_t(buf, 15, request_urgency);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL, buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC);
#else
    mavlink_fleet_uav_request_local_t packet;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.request_type = request_type;
    packet.request_ID = request_ID;
    packet.ID_to_replace = ID_to_replace;
    packet.request_urgency = request_urgency;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL, (const char *)&packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC);
#endif
}

/**
 * @brief Send a fleet_uav_request_local message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_uav_request_local_send_struct(mavlink_channel_t chan, const mavlink_fleet_uav_request_local_t* fleet_uav_request_local)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_uav_request_local_send(chan, fleet_uav_request_local->request_type, fleet_uav_request_local->request_ID, fleet_uav_request_local->ID_to_replace, fleet_uav_request_local->x, fleet_uav_request_local->y, fleet_uav_request_local->z, fleet_uav_request_local->request_urgency);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL, (const char *)fleet_uav_request_local, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_uav_request_local_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t request_type, uint8_t request_ID, uint8_t ID_to_replace, float x, float y, float z, uint8_t request_urgency)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_float(buf, 0, x);
    _mav_put_float(buf, 4, y);
    _mav_put_float(buf, 8, z);
    _mav_put_uint8_t(buf, 12, request_type);
    _mav_put_uint8_t(buf, 13, request_ID);
    _mav_put_uint8_t(buf, 14, ID_to_replace);
    _mav_put_uint8_t(buf, 15, request_urgency);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL, buf, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC);
#else
    mavlink_fleet_uav_request_local_t *packet = (mavlink_fleet_uav_request_local_t *)msgbuf;
    packet->x = x;
    packet->y = y;
    packet->z = z;
    packet->request_type = request_type;
    packet->request_ID = request_ID;
    packet->ID_to_replace = ID_to_replace;
    packet->request_urgency = request_urgency;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL, (const char *)packet, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_MIN_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_UAV_REQUEST_LOCAL UNPACKING


/**
 * @brief Get field request_type from fleet_uav_request_local message
 *
 * @return  Type of the request (consult ENUM_FLEET_REQUEST_TYPE)
 */
static inline uint8_t mavlink_msg_fleet_uav_request_local_get_request_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  12);
}

/**
 * @brief Get field request_ID from fleet_uav_request_local message
 *
 * @return  Request's unique ID
 */
static inline uint8_t mavlink_msg_fleet_uav_request_local_get_request_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  13);
}

/**
 * @brief Get field ID_to_replace from fleet_uav_request_local message
 *
 * @return  System ID of the system/component that sent the request
 */
static inline uint8_t mavlink_msg_fleet_uav_request_local_get_ID_to_replace(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  14);
}

/**
 * @brief Get field x from fleet_uav_request_local message
 *
 * @return  Coordinate X
 */
static inline float mavlink_msg_fleet_uav_request_local_get_x(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  0);
}

/**
 * @brief Get field y from fleet_uav_request_local message
 *
 * @return  Coordinate Y
 */
static inline float mavlink_msg_fleet_uav_request_local_get_y(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field z from fleet_uav_request_local message
 *
 * @return  Coordinate Z
 */
static inline float mavlink_msg_fleet_uav_request_local_get_z(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field request_urgency from fleet_uav_request_local message
 *
 * @return  Parameter used as a coefficient for choosing the most critical request to respond to
 */
static inline uint8_t mavlink_msg_fleet_uav_request_local_get_request_urgency(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  15);
}

/**
 * @brief Decode a fleet_uav_request_local message into a struct
 *
 * @param msg The message to decode
 * @param fleet_uav_request_local C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_uav_request_local_decode(const mavlink_message_t* msg, mavlink_fleet_uav_request_local_t* fleet_uav_request_local)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_uav_request_local->x = mavlink_msg_fleet_uav_request_local_get_x(msg);
    fleet_uav_request_local->y = mavlink_msg_fleet_uav_request_local_get_y(msg);
    fleet_uav_request_local->z = mavlink_msg_fleet_uav_request_local_get_z(msg);
    fleet_uav_request_local->request_type = mavlink_msg_fleet_uav_request_local_get_request_type(msg);
    fleet_uav_request_local->request_ID = mavlink_msg_fleet_uav_request_local_get_request_ID(msg);
    fleet_uav_request_local->ID_to_replace = mavlink_msg_fleet_uav_request_local_get_ID_to_replace(msg);
    fleet_uav_request_local->request_urgency = mavlink_msg_fleet_uav_request_local_get_request_urgency(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN? msg->len : MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN;
        memset(fleet_uav_request_local, 0, MAVLINK_MSG_ID_FLEET_UAV_REQUEST_LOCAL_LEN);
    memcpy(fleet_uav_request_local, _MAV_PAYLOAD(msg), len);
#endif
}
