#pragma once
// MESSAGE FLEET_SET_TARGET PACKING

#define MAVLINK_MSG_ID_FLEET_SET_TARGET 175

MAVPACKED(
typedef struct __mavlink_fleet_set_target_t {
 uint32_t latitude; /*<  Target latitude*/
 uint32_t longitude; /*<  Target longitude*/
 float perimeter; /*<  Target perimeter in meters (radius of the ring formation)*/
 uint16_t altitude; /*<  Target altitude (GPS)*/
 uint16_t relative_alt; /*<  Target altitude from ground*/
 int16_t heading; /*<  Target heading (used for the point of view)*/
 uint16_t drone_count; /*<  Number of drones needed for the current target*/
 uint8_t target_ID; /*<  Unique ID of the fleet target*/
 uint8_t target_type; /*<  Target type (consult ENUM_FLEET_TARGET_TYPE)*/
 uint8_t add_replace; /*<  If the target_ID already exists, replace it, otherwise add to the target to the end of the target queue*/
}) mavlink_fleet_set_target_t;

#define MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN 23
#define MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN 23
#define MAVLINK_MSG_ID_175_LEN 23
#define MAVLINK_MSG_ID_175_MIN_LEN 23

#define MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC 164
#define MAVLINK_MSG_ID_175_CRC 164



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLEET_SET_TARGET { \
    175, \
    "FLEET_SET_TARGET", \
    10, \
    {  { "latitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_set_target_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_fleet_set_target_t, longitude) }, \
         { "perimeter", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_fleet_set_target_t, perimeter) }, \
         { "altitude", NULL, MAVLINK_TYPE_UINT16_T, 0, 12, offsetof(mavlink_fleet_set_target_t, altitude) }, \
         { "relative_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 14, offsetof(mavlink_fleet_set_target_t, relative_alt) }, \
         { "heading", NULL, MAVLINK_TYPE_INT16_T, 0, 16, offsetof(mavlink_fleet_set_target_t, heading) }, \
         { "drone_count", NULL, MAVLINK_TYPE_UINT16_T, 0, 18, offsetof(mavlink_fleet_set_target_t, drone_count) }, \
         { "target_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 20, offsetof(mavlink_fleet_set_target_t, target_ID) }, \
         { "target_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 21, offsetof(mavlink_fleet_set_target_t, target_type) }, \
         { "add_replace", NULL, MAVLINK_TYPE_UINT8_T, 0, 22, offsetof(mavlink_fleet_set_target_t, add_replace) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLEET_SET_TARGET { \
    "FLEET_SET_TARGET", \
    10, \
    {  { "latitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_fleet_set_target_t, latitude) }, \
         { "longitude", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_fleet_set_target_t, longitude) }, \
         { "perimeter", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_fleet_set_target_t, perimeter) }, \
         { "altitude", NULL, MAVLINK_TYPE_UINT16_T, 0, 12, offsetof(mavlink_fleet_set_target_t, altitude) }, \
         { "relative_alt", NULL, MAVLINK_TYPE_UINT16_T, 0, 14, offsetof(mavlink_fleet_set_target_t, relative_alt) }, \
         { "heading", NULL, MAVLINK_TYPE_INT16_T, 0, 16, offsetof(mavlink_fleet_set_target_t, heading) }, \
         { "drone_count", NULL, MAVLINK_TYPE_UINT16_T, 0, 18, offsetof(mavlink_fleet_set_target_t, drone_count) }, \
         { "target_ID", NULL, MAVLINK_TYPE_UINT8_T, 0, 20, offsetof(mavlink_fleet_set_target_t, target_ID) }, \
         { "target_type", NULL, MAVLINK_TYPE_UINT8_T, 0, 21, offsetof(mavlink_fleet_set_target_t, target_type) }, \
         { "add_replace", NULL, MAVLINK_TYPE_UINT8_T, 0, 22, offsetof(mavlink_fleet_set_target_t, add_replace) }, \
         } \
}
#endif

/**
 * @brief Pack a fleet_set_target message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target_ID  Unique ID of the fleet target
 * @param target_type  Target type (consult ENUM_FLEET_TARGET_TYPE)
 * @param latitude  Target latitude
 * @param longitude  Target longitude
 * @param altitude  Target altitude (GPS)
 * @param relative_alt  Target altitude from ground
 * @param heading  Target heading (used for the point of view)
 * @param add_replace  If the target_ID already exists, replace it, otherwise add to the target to the end of the target queue
 * @param drone_count  Number of drones needed for the current target
 * @param perimeter  Target perimeter in meters (radius of the ring formation)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_set_target_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target_ID, uint8_t target_type, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading, uint8_t add_replace, uint16_t drone_count, float perimeter)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN];
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_float(buf, 8, perimeter);
    _mav_put_uint16_t(buf, 12, altitude);
    _mav_put_uint16_t(buf, 14, relative_alt);
    _mav_put_int16_t(buf, 16, heading);
    _mav_put_uint16_t(buf, 18, drone_count);
    _mav_put_uint8_t(buf, 20, target_ID);
    _mav_put_uint8_t(buf, 21, target_type);
    _mav_put_uint8_t(buf, 22, add_replace);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN);
#else
    mavlink_fleet_set_target_t packet;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.perimeter = perimeter;
    packet.altitude = altitude;
    packet.relative_alt = relative_alt;
    packet.heading = heading;
    packet.drone_count = drone_count;
    packet.target_ID = target_ID;
    packet.target_type = target_type;
    packet.add_replace = add_replace;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_SET_TARGET;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC);
}

/**
 * @brief Pack a fleet_set_target message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_ID  Unique ID of the fleet target
 * @param target_type  Target type (consult ENUM_FLEET_TARGET_TYPE)
 * @param latitude  Target latitude
 * @param longitude  Target longitude
 * @param altitude  Target altitude (GPS)
 * @param relative_alt  Target altitude from ground
 * @param heading  Target heading (used for the point of view)
 * @param add_replace  If the target_ID already exists, replace it, otherwise add to the target to the end of the target queue
 * @param drone_count  Number of drones needed for the current target
 * @param perimeter  Target perimeter in meters (radius of the ring formation)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_fleet_set_target_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target_ID,uint8_t target_type,uint32_t latitude,uint32_t longitude,uint16_t altitude,uint16_t relative_alt,int16_t heading,uint8_t add_replace,uint16_t drone_count,float perimeter)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN];
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_float(buf, 8, perimeter);
    _mav_put_uint16_t(buf, 12, altitude);
    _mav_put_uint16_t(buf, 14, relative_alt);
    _mav_put_int16_t(buf, 16, heading);
    _mav_put_uint16_t(buf, 18, drone_count);
    _mav_put_uint8_t(buf, 20, target_ID);
    _mav_put_uint8_t(buf, 21, target_type);
    _mav_put_uint8_t(buf, 22, add_replace);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN);
#else
    mavlink_fleet_set_target_t packet;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.perimeter = perimeter;
    packet.altitude = altitude;
    packet.relative_alt = relative_alt;
    packet.heading = heading;
    packet.drone_count = drone_count;
    packet.target_ID = target_ID;
    packet.target_type = target_type;
    packet.add_replace = add_replace;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLEET_SET_TARGET;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC);
}

/**
 * @brief Encode a fleet_set_target struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param fleet_set_target C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_set_target_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_fleet_set_target_t* fleet_set_target)
{
    return mavlink_msg_fleet_set_target_pack(system_id, component_id, msg, fleet_set_target->target_ID, fleet_set_target->target_type, fleet_set_target->latitude, fleet_set_target->longitude, fleet_set_target->altitude, fleet_set_target->relative_alt, fleet_set_target->heading, fleet_set_target->add_replace, fleet_set_target->drone_count, fleet_set_target->perimeter);
}

/**
 * @brief Encode a fleet_set_target struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param fleet_set_target C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_fleet_set_target_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_fleet_set_target_t* fleet_set_target)
{
    return mavlink_msg_fleet_set_target_pack_chan(system_id, component_id, chan, msg, fleet_set_target->target_ID, fleet_set_target->target_type, fleet_set_target->latitude, fleet_set_target->longitude, fleet_set_target->altitude, fleet_set_target->relative_alt, fleet_set_target->heading, fleet_set_target->add_replace, fleet_set_target->drone_count, fleet_set_target->perimeter);
}

/**
 * @brief Send a fleet_set_target message
 * @param chan MAVLink channel to send the message
 *
 * @param target_ID  Unique ID of the fleet target
 * @param target_type  Target type (consult ENUM_FLEET_TARGET_TYPE)
 * @param latitude  Target latitude
 * @param longitude  Target longitude
 * @param altitude  Target altitude (GPS)
 * @param relative_alt  Target altitude from ground
 * @param heading  Target heading (used for the point of view)
 * @param add_replace  If the target_ID already exists, replace it, otherwise add to the target to the end of the target queue
 * @param drone_count  Number of drones needed for the current target
 * @param perimeter  Target perimeter in meters (radius of the ring formation)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_fleet_set_target_send(mavlink_channel_t chan, uint8_t target_ID, uint8_t target_type, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading, uint8_t add_replace, uint16_t drone_count, float perimeter)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN];
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_float(buf, 8, perimeter);
    _mav_put_uint16_t(buf, 12, altitude);
    _mav_put_uint16_t(buf, 14, relative_alt);
    _mav_put_int16_t(buf, 16, heading);
    _mav_put_uint16_t(buf, 18, drone_count);
    _mav_put_uint8_t(buf, 20, target_ID);
    _mav_put_uint8_t(buf, 21, target_type);
    _mav_put_uint8_t(buf, 22, add_replace);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_TARGET, buf, MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC);
#else
    mavlink_fleet_set_target_t packet;
    packet.latitude = latitude;
    packet.longitude = longitude;
    packet.perimeter = perimeter;
    packet.altitude = altitude;
    packet.relative_alt = relative_alt;
    packet.heading = heading;
    packet.drone_count = drone_count;
    packet.target_ID = target_ID;
    packet.target_type = target_type;
    packet.add_replace = add_replace;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_TARGET, (const char *)&packet, MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC);
#endif
}

/**
 * @brief Send a fleet_set_target message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_fleet_set_target_send_struct(mavlink_channel_t chan, const mavlink_fleet_set_target_t* fleet_set_target)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_fleet_set_target_send(chan, fleet_set_target->target_ID, fleet_set_target->target_type, fleet_set_target->latitude, fleet_set_target->longitude, fleet_set_target->altitude, fleet_set_target->relative_alt, fleet_set_target->heading, fleet_set_target->add_replace, fleet_set_target->drone_count, fleet_set_target->perimeter);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_TARGET, (const char *)fleet_set_target, MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_fleet_set_target_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target_ID, uint8_t target_type, uint32_t latitude, uint32_t longitude, uint16_t altitude, uint16_t relative_alt, int16_t heading, uint8_t add_replace, uint16_t drone_count, float perimeter)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, latitude);
    _mav_put_uint32_t(buf, 4, longitude);
    _mav_put_float(buf, 8, perimeter);
    _mav_put_uint16_t(buf, 12, altitude);
    _mav_put_uint16_t(buf, 14, relative_alt);
    _mav_put_int16_t(buf, 16, heading);
    _mav_put_uint16_t(buf, 18, drone_count);
    _mav_put_uint8_t(buf, 20, target_ID);
    _mav_put_uint8_t(buf, 21, target_type);
    _mav_put_uint8_t(buf, 22, add_replace);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_TARGET, buf, MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC);
#else
    mavlink_fleet_set_target_t *packet = (mavlink_fleet_set_target_t *)msgbuf;
    packet->latitude = latitude;
    packet->longitude = longitude;
    packet->perimeter = perimeter;
    packet->altitude = altitude;
    packet->relative_alt = relative_alt;
    packet->heading = heading;
    packet->drone_count = drone_count;
    packet->target_ID = target_ID;
    packet->target_type = target_type;
    packet->add_replace = add_replace;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLEET_SET_TARGET, (const char *)packet, MAVLINK_MSG_ID_FLEET_SET_TARGET_MIN_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN, MAVLINK_MSG_ID_FLEET_SET_TARGET_CRC);
#endif
}
#endif

#endif

// MESSAGE FLEET_SET_TARGET UNPACKING


/**
 * @brief Get field target_ID from fleet_set_target message
 *
 * @return  Unique ID of the fleet target
 */
static inline uint8_t mavlink_msg_fleet_set_target_get_target_ID(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  20);
}

/**
 * @brief Get field target_type from fleet_set_target message
 *
 * @return  Target type (consult ENUM_FLEET_TARGET_TYPE)
 */
static inline uint8_t mavlink_msg_fleet_set_target_get_target_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  21);
}

/**
 * @brief Get field latitude from fleet_set_target message
 *
 * @return  Target latitude
 */
static inline uint32_t mavlink_msg_fleet_set_target_get_latitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field longitude from fleet_set_target message
 *
 * @return  Target longitude
 */
static inline uint32_t mavlink_msg_fleet_set_target_get_longitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Get field altitude from fleet_set_target message
 *
 * @return  Target altitude (GPS)
 */
static inline uint16_t mavlink_msg_fleet_set_target_get_altitude(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  12);
}

/**
 * @brief Get field relative_alt from fleet_set_target message
 *
 * @return  Target altitude from ground
 */
static inline uint16_t mavlink_msg_fleet_set_target_get_relative_alt(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  14);
}

/**
 * @brief Get field heading from fleet_set_target message
 *
 * @return  Target heading (used for the point of view)
 */
static inline int16_t mavlink_msg_fleet_set_target_get_heading(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  16);
}

/**
 * @brief Get field add_replace from fleet_set_target message
 *
 * @return  If the target_ID already exists, replace it, otherwise add to the target to the end of the target queue
 */
static inline uint8_t mavlink_msg_fleet_set_target_get_add_replace(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  22);
}

/**
 * @brief Get field drone_count from fleet_set_target message
 *
 * @return  Number of drones needed for the current target
 */
static inline uint16_t mavlink_msg_fleet_set_target_get_drone_count(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  18);
}

/**
 * @brief Get field perimeter from fleet_set_target message
 *
 * @return  Target perimeter in meters (radius of the ring formation)
 */
static inline float mavlink_msg_fleet_set_target_get_perimeter(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Decode a fleet_set_target message into a struct
 *
 * @param msg The message to decode
 * @param fleet_set_target C-struct to decode the message contents into
 */
static inline void mavlink_msg_fleet_set_target_decode(const mavlink_message_t* msg, mavlink_fleet_set_target_t* fleet_set_target)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    fleet_set_target->latitude = mavlink_msg_fleet_set_target_get_latitude(msg);
    fleet_set_target->longitude = mavlink_msg_fleet_set_target_get_longitude(msg);
    fleet_set_target->perimeter = mavlink_msg_fleet_set_target_get_perimeter(msg);
    fleet_set_target->altitude = mavlink_msg_fleet_set_target_get_altitude(msg);
    fleet_set_target->relative_alt = mavlink_msg_fleet_set_target_get_relative_alt(msg);
    fleet_set_target->heading = mavlink_msg_fleet_set_target_get_heading(msg);
    fleet_set_target->drone_count = mavlink_msg_fleet_set_target_get_drone_count(msg);
    fleet_set_target->target_ID = mavlink_msg_fleet_set_target_get_target_ID(msg);
    fleet_set_target->target_type = mavlink_msg_fleet_set_target_get_target_type(msg);
    fleet_set_target->add_replace = mavlink_msg_fleet_set_target_get_add_replace(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN? msg->len : MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN;
        memset(fleet_set_target, 0, MAVLINK_MSG_ID_FLEET_SET_TARGET_LEN);
    memcpy(fleet_set_target, _MAV_PAYLOAD(msg), len);
#endif
}
