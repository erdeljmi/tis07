// Milan Erdelj
// 28/12/2010

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <time.h>
#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <termios.h>

#define BUFFER_LEN 8
//uchar add_checksum(uchar *buff);
struct sockaddr_in myaddr_out;
struct sockaddr_in client_out;
int socket_out;
char buffer_out[BUFFER_LEN];
char local_time[25];
time_t now;
struct timeval tv;
struct timezone tz;
struct tm *tm;

int main(int argc, char *argv[])
{
    int clilen,broadcast=1;
    time(&now);
    tm = localtime(&now);
    strftime(local_time,25,"%x_%X",tm);
    printf("local time: %s\n",local_time);
    clilen=sizeof(client_out);
    if((socket_out=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP))<0)
    {
        printf("udp out socket error\n");
	exit(1);
    }
    if((setsockopt(socket_out,SOL_SOCKET,SO_BROADCAST,&broadcast,sizeof broadcast)) == -1)
    {
        printf("udp out setsockopt error\n");
	exit(1);
    }
    memset((char *) &myaddr_out, 0, sizeof(myaddr_out));
    myaddr_out.sin_family=AF_INET;
    myaddr_out.sin_port=htons(15000);
    myaddr_out.sin_addr.s_addr = inet_addr("192.168.6.255");
    //sprintf(buffer_out,"udp broadcast");
    gettimeofday(&tv, &tz);
    buffer_out[0] = (tv.tv_sec)>>24;
    buffer_out[1] = (tv.tv_sec)>>16;
    buffer_out[2] = (tv.tv_sec)>>8;
    buffer_out[3] = tv.tv_sec;
    buffer_out[4] = (tv.tv_usec)>>24;
    buffer_out[5] = (tv.tv_usec)>>16;
    buffer_out[6] = (tv.tv_usec)>>8;
    buffer_out[7] = tv.tv_usec;
    //sprintf(buffer_out,"%ld.%ld",tv.tv_sec,tv.tv_usec);
    sendto(socket_out, buffer_out, BUFFER_LEN, 0, (struct sockaddr *) &myaddr_out, sizeof(myaddr_out));
    printf("time: %ld.%ld\n", tv.tv_sec, tv.tv_usec);
    printf("buffer: %ld.%ld\n", ((long)buffer_out[0]<<24)+((long)buffer_out[1]<<16)+((long)buffer_out[2]<<8)+(long)buffer_out[3], ((long)buffer_out[4]<<24)+((long)buffer_out[5]<<16)+((long)buffer_out[6]<<8)+(long)buffer_out[7]);
    printf("size: %ld\n",sizeof(tv.tv_sec));
    return 0;
}
